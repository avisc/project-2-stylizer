# ------------------------------------------------------------
# Real-Time Style Transfer Implementation
# Licensed under The MIT License [see LICENSE for details]
# Written by Cheng-Bin Jin, based on code from Logan Engstrom
# Email: sbkim0407@gmail.com
# ------------------------------------------------------------
#deploy python server
from sanic import Sanic
from sanic.response import json
app = Sanic("App Name")

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
import time
import numpy as np
import tensorflow as tf
from collections import defaultdict

from style_transfer import Transfer
import utils as utils

########### Import ##########
import json as read_json
from torchvision import models
from PIL import Image
import matplotlib.pyplot as plt
import torch
import cv2
# Apply the transformations needed
import torchvision.transforms as T

from sanic.response import text

FLAGS = tf.compat.v1.flags.FLAGS
tf.compat.v1.flags.DEFINE_string('gpu_index', '0', 'gpu index, default: 0')
tf.compat.v1.flags.DEFINE_string('checkpoint_dir', 'checkpoints/',
                    'dir to read checkpoint in, default: ./checkpoints/aquarelle')

tf.compat.v1.flags.DEFINE_string('in_path', '', 'test image path, default: ./examples/test/')
tf.compat.v1.flags.DEFINE_string('out_path', '../server/creations/output',
                    'destination dir of transformed files, default: ./examples/restuls')

@app.route("/transfer")
def transfer(request):
    #define transfer variables
    transfer_variables = request.get_args(keep_blank_values=True)
    checkpoint_dir_variable = ''.join(transfer_variables['checkpoint_dir'])
    in_path_variable = ''.join(transfer_variables['in_path'])
    #transfer / background
    out_path_type_variable = ''.join(transfer_variables['out_path'])

    #FLAGS = tf.flags.FLAGS
    FLAGS.checkpoint_dir = './checkpoints/'+ checkpoint_dir_variable
    FLAGS.in_path = '../server/creations/' + in_path_variable
    FLAGS.out_path = '../server/creations/'+ out_path_type_variable 
    print('in_path: ', FLAGS.in_path)
    print('checkpoint_dir: ', FLAGS.checkpoint_dir)
    print('output_dir: ', FLAGS.out_path)
    #main function
    os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
    check_opts(FLAGS)
    style_name = FLAGS.checkpoint_dir.split('/')[-1]
    img_paths = utils.all_files_under(FLAGS.in_path)
    out_paths = [os.path.join(FLAGS.out_path, style_name, file)
                for file in utils.all_files_under(FLAGS.in_path, append_path=False)]
    feed_forward(img_paths, out_paths, FLAGS.checkpoint_dir)
    
    return json({
        "parsed": True,
        "url": request.url,
        "transfer_variables": transfer_variables,
        "query_string": request.query_string
    })


def feed_transform(data_in, paths_out, checkpoint_dir):
    print("run feed_transform")
    img_shape = utils.imread(data_in[0]).shape

    g = tf.Graph()
    soft_config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
    soft_config.gpu_options.allow_growth = True

    with g.as_default(), tf.compat.v1.Session(config=soft_config) as sess:
        img_placeholder = tf.compat.v1.placeholder(tf.float32, shape=[None, *img_shape], name='img_placeholder')

        model = Transfer()
        pred = model(img_placeholder)

        saver = tf.compat.v1.train.Saver()
        if os.path.isdir(checkpoint_dir):
            ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
            else:
                raise Exception('No checkpoint found...')
        else:
            saver.restore(sess, checkpoint_dir)

        img = np.asarray([utils.imread(data_in[0])]).astype(np.float32)
        start_tic = time.time()
        _pred = sess.run(pred, feed_dict={img_placeholder: img})
        end_toc = time.time()
        print('PT: {:.2f} msec.\n'.format((end_toc - start_tic) * 1000))
        utils.imsave(paths_out[0], _pred[0])  # paths_out and _pred is list


def feed_forward(in_paths, out_paths, checkpoint_dir):
    print("run feed_forward")
    print("in_paths", in_paths)
    print("out_paths", out_paths)
    print("checkpoint_dir", checkpoint_dir)
    in_path_of_shape = defaultdict(list)
    out_path_of_shape = defaultdict(list)

    for idx in range(len(in_paths)):
        in_image = in_paths[idx]
        out_image = out_paths[idx]

        shape = "%dx%dx%d" % utils.imread(in_image).shape
        in_path_of_shape[shape].append(in_image)
        out_path_of_shape[shape].append(out_image)

    for shape in in_path_of_shape:
        print('Processing images of shape {}'.format(shape))
        feed_transform(in_path_of_shape[shape], out_path_of_shape[shape], checkpoint_dir)



def check_opts(flags):
    print('run check_opts')
    utils.exists(flags.checkpoint_dir, 'checkpoint_dir not found!')
    utils.exists(flags.in_path, 'in_path not found!')

    style_name = FLAGS.checkpoint_dir.split('/')[-1]
    if not os.path.isdir(os.path.join(flags.out_path, style_name)):
        os.makedirs(os.path.join(flags.out_path, style_name))

################################# Background change ################################
# conda install -c anaconda pillow
# conda install -c conda-forge sanic

############install in conda for background change
# conda install -c pytorch torchvision
# conda install -c conda-forge matplotlib 
# conda install -c conda-forge opencv




# Define the helper function
def decode_segmap(image, source, bgimg, nc=21):
  
  label_colors = np.array([(0, 0, 0),  # 0=background
               # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
               (128, 0, 0), (0, 128, 0), (128, 128, 0), (0, 0, 128), (128, 0, 128),
               # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
               (0, 128, 128), (128, 128, 128), (64, 0, 0), (192, 0, 0), (64, 128, 0),
               # 11=dining table, 12=dog, 13=horse, 14=motorbike, 15=person
               (192, 128, 0), (64, 0, 128), (192, 0, 128), (64, 128, 128), (192, 128, 128),
               # 16=potted plant, 17=sheep, 18=sofa, 19=train, 20=tv/monitor
               (0, 64, 0), (128, 64, 0), (0, 192, 0), (128, 192, 0), (0, 64, 128)])

  r = np.zeros_like(image).astype(np.uint8)
  g = np.zeros_like(image).astype(np.uint8)
  b = np.zeros_like(image).astype(np.uint8)
  
  for l in range(0, nc):
    idx = image == l
    r[idx] = label_colors[l, 0]
    g[idx] = label_colors[l, 1]
    b[idx] = label_colors[l, 2]
    
    
  rgb = np.stack([r, g, b], axis=2)
  
  # Load the foreground input image 
  foreground = cv2.imread(source)

  # Load the background input image 
  background = cv2.imread(bgimg)

  # Change the color of foreground image to RGB 
  # and resize images to match shape of R-band in RGB output map
  foreground = cv2.cvtColor(foreground, cv2.COLOR_BGR2RGB)
  background = cv2.cvtColor(background, cv2.COLOR_BGR2RGB)
  foreground = cv2.resize(foreground,(r.shape[1],r.shape[0]))
  background = cv2.resize(background,(r.shape[1],r.shape[0]))
  

  # Convert uint8 to float
  foreground = foreground.astype(float)
  background = background.astype(float)

  # Create a binary mask of the RGB output map using the threshold value 0
  th, alpha = cv2.threshold(np.array(rgb),0,255, cv2.THRESH_BINARY)

  # Apply a slight blur to the mask to soften edges
  alpha = cv2.GaussianBlur(alpha, (7,7),0)

  # Normalize the alpha mask to keep intensity between 0 and 1
  alpha = alpha.astype(float)/255

  # Multiply the foreground with the alpha matte
  foreground = cv2.multiply(alpha, foreground)  
  
  # Multiply the background with ( 1 - alpha )
  background = cv2.multiply(1.0 - alpha, background)  
  
  # Add the masked foreground and background
  outImage = cv2.add(foreground, background)

  # Return a normalized output image for display
  return outImage/255

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# import time

def segment(net, path, bgimagepath, show_orig=True, dev=device):
  img = Image.open(path).convert('RGB')
  
  if show_orig: plt.imshow(img); plt.axis('off'); plt.show()
  # Comment the Resize and CenterCrop for better inference results
  trf = T.Compose([T.Resize(400), 
                   #T.CenterCrop(224), 
                   T.ToTensor(), 
                   T.Normalize(mean = [0.485, 0.456, 0.406], 
                               std = [0.229, 0.224, 0.225])])
  inp = trf(img).unsqueeze(0).to(dev)
  out = net.to(dev)(inp)['out']
  om = torch.argmax(out.squeeze(), dim=0).detach().cpu().numpy()
  
  rgb = decode_segmap(om, path, bgimagepath)

#   # Get current timestamp
#   ts = time.time()

#   # Turn to string
#   ts = str(int(ts))

  filename_base = os.path.basename(path)

  # Set output path
  output_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'server', 'creations', 'bgOutput', filename_base))

  # Save image 
  plt.imsave(output_path, rgb)

#   plt.imshow(rgb); plt.axis('off'); plt.show()

  return output_path


@app.route("/background", methods=["POST",])
def background(request):
    # decode request body from b to string
    req_body_json = (request.body).decode('utf-8')
    # change from json to dict
    req_body_dict = read_json.loads(req_body_json)
    # get value of bg file path
    bg_file_path = req_body_dict["bgFilePath"]
    img_path = req_body_dict["imgPath"]

    print(img_path)
    print(bg_file_path)

    dlab = models.segmentation.deeplabv3_resnet101(pretrained=1).eval()

    result = segment(dlab, img_path,bg_file_path, show_orig=False)

    url = 'http://localhost:8080/api/v1/background'

    print(result)

    return json({
        "parsed": True,
        "url": url,
        "img_path": result
    })
    # Need to conda install -c anaconda requests
    # requests.post(url, json = {'path': result})

#import tensorflow.compat.v1 as tf
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000)
    

    
