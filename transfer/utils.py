# ------------------------------------------------------------
# Real-Time Style Transfer Implementation
# Licensed under The MIT License [see LICENSE for details]
# Written by Cheng-Bin Jin
# Email: sbkim0407@gmail.com
# ------------------------------------------------------------
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
import sys
import scipy.misc
import numpy as np


def imread(path, is_gray_scale=False, img_size=None):
    if is_gray_scale:
        img = scipy.misc.imread(path, flatten=True).astype(np.float32)
    else:
        img = scipy.misc.imread(path, mode='RGB').astype(np.float32)

        if not (img.ndim == 3 and img.shape[2] == 3):
            img = np.dstack((img, img, img))

    if img_size is not None:
        img = scipy.misc.imresize(img, img_size)

    return img


def imsave(path, img):
    img = np.clip(img, 0, 255).astype(np.uint8)
    scipy.misc.imsave(path, img)


def all_files_under(path, extension=None, append_path=True, sort=True):
    print("all_files_under function, os.listdir(path)", os.listdir(path))
    if append_path:
        if extension is None:
            print("all_files_under function: extension is None (append_path)")
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            print("all_files_under function: extension is NOT None (append_path)")
            filenames = [os.path.join(path, fname)
                         for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            print("all_files_under function: extension is None (NOT append_path)")
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            print("all_files_under function: extension is NOT None (NOT append_path)")
            filenames = [os.path.basename(fname)
                         for fname in os.listdir(path) if fname.endswith(extension)]

    if sort:
        filenames = sorted(filenames)

    print("all_files_under function, filenames", filenames)
    return filenames


def exists(p, msg):
    assert os.path.exists(p), msg


def print_metrics(itr, kargs):
    print("*** Iteration {}  ====> ".format(itr))
    for name, value in kargs.items():
        print("{} : {}, ".format(name, value))
    print("")
    sys.stdout.flush()

