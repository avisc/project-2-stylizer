let redirectPage = "";

// Pass the price ID of your customer’s selection to the backend endpoint that creates the Checkout Session:
let createCheckoutSession = async function (priceId) {
    let result = await fetch(`${apiVersion}/checkout`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            priceId: priceId,
        }),
    });

    if (result.status === 401) {
        showLogin();
        return;
    }

    return result.json();
};

// Add an event handler to the button to redirect to Checkout, calling redirectToCheckout and passing the Checkout Session ID:
const buttons = document.querySelectorAll(".card button");

for (let button of buttons) {
    button.addEventListener("click", async function (evt) {
        try {
            //Get the plan selected
            const plan = button.getAttribute("data-plan");

            //Get keys
            const keys = await getKey();

            //If not logged in
            if (keys["key"].status === 401) {
                showLogin();
                loginInfo(redirectCheckout, plan);
                return;
            }

            //Check if already have subscription
            const hasSubs = await hasSubscription();

            if (hasSubs === true) {
                alert("Already subscribed!");
                return;
            }

            //Redirect to correct page
            await redirectCheckout(plan, keys);
        } catch (err) {
            console.log(err);
        }
    });
}

async function getKey() {
    const key = await fetch(`${apiVersion}/checkout/key`);
    const keyData = await key.json();
    const publishableKey = keyData.publishableKey;
    const monthlyPlanId = keyData.monthlyPlanId;
    const yearlyPlanId = keyData.yearlyPlanId;
    return {
        key: key,
        publishableKey: publishableKey,
        monthlyPlanId: monthlyPlanId,
        yearlyPlanId: yearlyPlanId,
    };
}

async function redirectCheckout(plan, keys = null) {
    if (keys === null) {
        //Get keys
        keys = await getKey();
    }

    const { publishableKey, monthlyPlanId, yearlyPlanId } = keys;

    //Init stripe
    const stripe = Stripe(publishableKey);

    if (plan === "free") {
        window.location = "/profile/edit-profile.html";
        return;
    }

    if (plan === "monthly") {
        let data = await createCheckoutSession(monthlyPlanId);
        let result = stripe.redirectToCheckout({
            sessionId: data.sessionId,
        });
    } else {
        let data = await createCheckoutSession(yearlyPlanId);
        let result = stripe.redirectToCheckout({
            sessionId: data.sessionId,
        });
    }
}
