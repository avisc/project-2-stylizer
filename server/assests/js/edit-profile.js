//when content load
window.addEventListener("DOMContentLoaded", async () => {
    await getUserEmail();
    changePassword();
});

async function getUserEmail() {
    //Get email input field
    const emailField = document.querySelector('input[id="user-email"]');

    //Get user info
    const user = await fetch(`${apiVersion}/user/profile`);
    const userData = await user.json();

    //Set value of email field = user email
    emailField.value = userData.email;
}

function changePassword() {
    const changePasswordForm = document.querySelector("#edit-profile-form");

    changePasswordForm.addEventListener("submit", async (e) => {
        try {
            e.preventDefault();

            //1. Get input DOM ele
            const currentPassword = e.target.currentPassword;
            const newPassword = e.target.newPassword;
            const confirmPassword = e.target.confirmPassword;

            //2. Check if confirm password match with new password
            if (confirmPassword.value !== newPassword.value) {
                systemResponseContent.innerText =
                    "password doesn't match. Please try again.";
                systemResponse();

                //Clear password field
                currentPassword.value = "";
                newPassword.value = "";
                confirmPassword.value = "";
                return;
            }

            //3. send request to server
            const result = await fetch(`${apiVersion}/user/profile/edit`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    currentPassword: currentPassword.value,
                    newPassword: newPassword.value,
                }),
            });

            const resultData = await result.json();

            //If current password doesn't match password in DB
            if (result.status === 400) {
                systemResponseContent.innerText = resultData.message;
                systemResponse();
                return;
            }

            //4. display result
            changePasswordForm.reset();
            systemResponseContent.innerText = resultData.message;
            systemResponse();
            getUserEmail();
        } catch (err) {
            systemResponseContent.innerText =
                "An error has occurred. Please try again";
            systemResponse();

            console.error(err);
        } finally {
            //Clear password field
            currentPassword.value = "";
            newPassword.value = "";
            confirmPassword.value = "";
        }
    });
}
