// Landing Page
window.addEventListener("DOMContentLoaded", (event) => {
    bannerAnimation();
    slider();
    triggerFileUpload();
    triggerWebCam();
    scrollAnimation();
    mobileTouchAnimation();
    showNavImage();
});

/*----------------------------Animation--------------------------------------------------*/
// Landing Page Banner Animation
const navBanner = document.querySelector("#img-header");
const landingPageDiscoverBtn = document.querySelector("#scroll-load");
const body = document.querySelector("body");
function showNavImage() {
    landingPageDiscoverBtn.addEventListener("click", () => {
        body.style.overflow = "visible";
        navBanner.style.transform = "translateX(-105%)";
        navBanner.style.display = "none";

        document.querySelector("#progress-bar").classList.add("after-scroll");
        document.querySelector("#img-upload").classList.add("after-scroll");
    });
}
function scrollAnimation() {
    if (document.addEventListener) {
        document.addEventListener("mousewheel", MouseWheelHandler, false); //IE9, Chrome, Safari, Oper
        document.addEventListener("DOMMouseScroll", MouseWheelHandler, false); //Firefox
    } else {
        document.attachEvent("onmousewheel", MouseWheelHandler); //IE 6/7/8
    }

    //const i = 1;
    let mouseWheel = true;
    function MouseWheelHandler(e) {
        if (!mouseWheel) {
            return false;
        }
        mouseWheel = false;
        setTimeout(function () {
            mouseWheel = true;
        }, 3000); // Stop mouse wheel event for 3 seconds

        setTimeout(function () {
            body.style.overflow = "visible";
            navBanner.style.transform = "translateX(-105%)";
            navBanner.style.display = "none";

            document
                .querySelector("#progress-bar")
                .classList.add("after-scroll");
            document.querySelector("#img-upload").classList.add("after-scroll");
        }, 100);
    }
}
function mobileTouchAnimation() {
    let touchStartX = 0;
    let dist = 0;
    navBanner.addEventListener(
        "touchstart",
        function (event) {
            let touchObj = event.changedTouches[0]; // reference first touch point (ie: first finger)
            touchStartX = parseInt(touchObj.clientX); // get x position of touch point relative to left edge of browser
            event.preventDefault();
        },
        false
    );

    navBanner.addEventListener(
        "touchmove",
        function (event) {
            let touchObj = event.changedTouches[0]; // reference first touch point (ie: first finger)
            dist = parseInt(touchObj.clientX) - touchStartX;
            event.preventDefault();
        },
        false
    );
    navBanner.addEventListener(
        "touchmove",
        function (event) {
            if (dist > 0) {
                setTimeout(function () {
                    body.style.overflow = "visible";
                    navBanner.style.transform = "translateX(-105%)";
                    navBanner.style.display = "none";

                    document
                        .querySelector("#progress-bar")
                        .classList.add("after-scroll");
                    document
                        .querySelector("#img-upload")
                        .classList.add("after-scroll");
                }, 100);
            }
            event.preventDefault();
        },
        false
    );
}
function bannerAnimation() {
    {
        // Init
        const animatedArea = document.getElementById("animated-area");
        const inner = document.getElementById("inner");

        // Mouse
        const mouse = {
            _x: 0,
            _y: 0,
            x: 0,
            y: 0,
            updatePosition: function (event) {
                const e = event || window.event;
                this.x = e.clientX - this._x;
                this.y = (e.clientY - this._y) * -1;
            },
            setOrigin: function (e) {
                this._x = e.offsetLeft + Math.floor(e.offsetWidth / 2);
                this._y = e.offsetTop + Math.floor(e.offsetHeight / 2);
            },
            show: function () {
                return "(" + this.x + ", " + this.y + ")";
            },
        };

        // Track the mouse position relative to the center of the container.
        mouse.setOrigin(animatedArea);

        //----------------------------------------------------

        let counter = 0;
        let refreshRate = 10;
        let isTimeToUpdate = function () {
            return counter++ % refreshRate === 0;
        };

        //----------------------------------------------------

        const onMouseEnterHandler = function (event) {
            update(event);
        };

        const onMouseLeaveHandler = function () {
            inner.style = "";
        };

        const onMouseMoveHandler = function (event) {
            if (isTimeToUpdate()) {
                update(event);
            }
        };

        //----------------------------------------------------

        const update = function (event) {
            mouse.updatePosition(event);
            updateTransformStyle(
                (mouse.y / inner.offsetHeight / 2).toFixed(2),
                (mouse.x / inner.offsetWidth / 2).toFixed(2)
            );
        };

        const updateTransformStyle = function (x, y) {
            const style = "rotateX(" + x + "deg) rotateY(" + y + "deg)";
            inner.style.transform = style;
            //inner.style.webkitTransform = style;
            inner.style.mozTranform = style;
            inner.style.msTransform = style;
            inner.style.oTransform = style;
        };

        //--------------------------------------------------------

        animatedArea.onmousemove = onMouseMoveHandler;
        animatedArea.onmouseleave = onMouseLeaveHandler;
        animatedArea.onmouseenter = onMouseEnterHandler;
    }
}

/*-------------------------------WebCam/ Upload-----------------------------------------------*/
const triggerWebCamBtn = document.querySelector("#trigger-webcam");
const triggerFileUploadBtn = document.querySelector("#trigger-file-upload");
const deleteImgBtn = document.querySelector("#img-upload .upload-area i");
const fileUploadInput = document.querySelector("#file-upload-input");

function triggerFileUpload() {
    triggerFileUploadBtn.addEventListener("click", () => {
        fileUploadInput.click();
    });
}

// Preview Uploaded Image (trigger by html)
function preview_image(event) {
    const previewImg = document.querySelector("#preview-image");
    const reader = new FileReader();
    reader.onload = function () {
        previewImg.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    if (event.target.files[0]) {
        triggerWebCamBtn.style.display = "none";
        triggerFileUploadBtn.style.display = "none";
        deleteImgBtn.style.display = "block";

        deleteImgBtn.addEventListener("click", () => {
            fileUploadInput.value = "";
            previewImg.src = "";
            triggerWebCamBtn.style.display = "block";
            triggerFileUploadBtn.style.display = "block";
            deleteImgBtn.style.display = "none";
        });
    }
}

// About Webcam
let isTakingPhoto = false;
let isCapturedPhoto = false;
const uploadArea = document.querySelector(".upload-area");
const camera = document.querySelector(".camera");
const output = document.querySelector(".output");

let video = null;
let canvas = null;
let photo = null;
let startbutton = null;
video = document.getElementById("video");
canvas = document.getElementById("canvas");
photo = document.getElementById("photo");
startbutton = document.getElementById("startbutton");
backtouploadButton = document.getElementById("backtoupload");
resumeTakePhotoButton = document.getElementById("resumeTakePhoto");

function defineWebCamAction() {
    if (isTakingPhoto) {
        camera.style.display = "block";
        output.style.display = "block";
        video.style.display = "block";
        startbutton.style.display = "flex";
        photo.style.display = "block";
        backtouploadButton.style.display = "block";
        uploadArea.style.display = "none";

        if (isCapturedPhoto) {
            camera.style.display = "none";
            output.style.display = "block";
            resumeTakePhotoButton.style.display = "block";
        } else {
            camera.style.display = "block";
            output.style.display = "none";
        }
    } else {
        camera.style.display = "none";
        output.style.display = "none";
        uploadArea.style.display = "flex";
    }
}

function triggerWebCam() {
    triggerWebCamBtn.addEventListener("click", () => {
        isTakingPhoto = true;
        startup();
        defineWebCamAction();
    });
    backtouploadButton.addEventListener("click", () => {
        isTakingPhoto = false;
        clearphoto();
        defineWebCamAction();
        stopStreamedVideo(video);
    });
    // After captured photo and want to resume photo taking
    resumeTakePhotoButton.addEventListener("click", () => {
        isTakingPhoto = true;
        isCapturedPhoto = false;
        defineWebCamAction();
    });
}

let width = 500; // We will scale the photo width to this
let height = 0; // This will be computed based on the input stream
// |streaming| indicates whether or not we're currently streaming video from the camera. Obviously, we start at false.
let streaming = false;
function startup() {
    navigator.mediaDevices
        .getUserMedia({ video: true, audio: false })
        .then(function (stream) {
            video.srcObject = stream;
            video.play();
        })
        .catch(function (err) {
            console.log("An error occurred: " + err);
        });

    video.addEventListener(
        "canplay",
        function (ev) {
            if (!streaming) {
                height = video.videoHeight / (video.videoWidth / width);

                // Firefox currently has a bug where the height can't be read from
                // the video, so we will make assumptions if this happens.
                if (isNaN(height)) {
                    height = width / (4 / 3);
                }

                video.setAttribute("width", width);
                video.setAttribute("height", height);
                canvas.setAttribute("width", width);
                canvas.setAttribute("height", height);
                streaming = true;
            }
        },
        false
    );

    startbutton.addEventListener(
        "click",
        function (ev) {
            takepicture();
            ev.preventDefault();
        },
        false
    );
    clearphoto();
}

// Fill the photo with an indication that none has been captured.
function clearphoto() {
    var context = canvas.getContext("2d");
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas.width, canvas.height);

    var data = canvas.toDataURL("image/png");
    photo.setAttribute("src", data);
}

// Capture a photo by fetching the current contents of the video
// and drawing it into a canvas, then converting that to a PNG
// format data URL. By drawing it on an offscreen canvas and then
// drawing that to the screen, we can change its size and/or apply
// other changes before drawing it.
function takepicture() {
    isCapturedPhoto = true;
    defineWebCamAction();

    var context = canvas.getContext("2d");
    if (width && height) {
        canvas.width = width;
        canvas.height = height;
        context.drawImage(video, 0, 0, width, height);

        var data = canvas.toDataURL("image/png");
        photo.setAttribute("src", data);
    } else {
        clearphoto();
    }
}
function stopStreamedVideo(videoElem) {
    const stream = videoElem.srcObject;
    const tracks = stream.getTracks();

    tracks.forEach(function (track) {
        track.stop();
    });

    videoElem.srcObject = null;
    streaming = false;
}

/*-------------------------------Slider-----------------------------------------------*/
// Style Slider
const sliderOptions = [
    {
        filename: "africa",
        style_name: "Africa",
    },
    {
        filename: "aquarelle",
        style_name: "Aquarelle",
    },
    {
        filename: "bango",
        style_name: "Bango",
    },
    {
        filename: "chinese_style",
        style_name: "Chinese Style",
    },
    {
        filename: "hampson",
        style_name: "Hampson",
    },
    {
        filename: "la_muse",
        style_name: "La Muse",
    },
    {
        filename: "rain_princess",
        style_name: "Rain Princess",
    },
    {
        filename: "the_scream",
        style_name: "The Scream",
    },
    {
        filename: "the_shipwreck_of_the_minotaur",
        style_name: "The Shipwreck of the Minotaur",
    },
    {
        filename: "udnie",
        style_name: "Udnie",
    },
    {
        filename: "wave",
        style_name: "Wave",
    },
    {
        filename: "africa",
        style_name: "Africa",
    },
    {
        filename: "aquarelle",
        style_name: "Aquarelle",
    },
    {
        filename: "bango",
        style_name: "Bango",
    },
    {
        filename: "chinese_style",
        style_name: "Chinese Style",
    },
    {
        filename: "hampson",
        style_name: "Hampson",
    },
    {
        filename: "la_muse",
        style_name: "La Muse",
    },
    {
        filename: "rain_princess",
        style_name: "Rain Princess",
    },
    {
        filename: "the_scream",
        style_name: "The Scream",
    },
    {
        filename: "the_shipwreck_of_the_minotaur",
        style_name: "The Shipwreck of the Minotaur",
    },
    {
        filename: "udnie",
        style_name: "Udnie",
    },
    {
        filename: "wave",
        style_name: "Wave",
    },
];

const styleSlider = document.querySelector("#select-style .swiper-wrapper");
let styleSliderContent = document.querySelector(
    "#select-style .swiper-wrapper div p"
);
function slider() {
    var swiper = new Swiper(".swiper-container", {
        effect: "coverflow",
        grabCursor: false,
        centeredSlides: false,
        //loop: true,
        slidesPerView: "auto",
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
        },
        coverflowEffect: {
            rotate: 25,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
        },
    });
}

for (let i = 0; i < sliderOptions.length; i++) {
    styleSlider.innerHTML += `                
    <a
    data-id="${sliderOptions[i]["filename"]}"
        
    class="swiper-slide"
    style="background-image: url(./img/style/${sliderOptions[i]["filename"]}.jpg)">
    <p>${sliderOptions[i]["style_name"]}</p>
    </a>`;
}

/*-------------------------------Select Style-----------------------------------------------*/
const styleSliderEles = document.querySelectorAll(
    "#select-style .swiper-wrapper a"
);
const loadingEffect = document.querySelector("#load-effect");
let styleName;

for (let styleSliderEle of styleSliderEles) {
    styleSliderEle.addEventListener("click", async (event) => {
        event.preventDefault();
        styleName = styleSliderEle.dataset.id;
        if (fileUploadInput.files[0]) {
            // Show Loading Effect
            loadingEffect.style.display = "block";

            // Image from file upload
            const formData = new FormData();
            formData.append("image", fileUploadInput.files[0]);

            const res = await fetch(
                `${apiVersion}/creation/${styleName}/style/${styleName}/fileType/fileimage`,
                {
                    method: "POST",
                    body: formData,
                }
            );

            const result = await res.json();
            if (res.status === 200) {
                window.location = `/creation.html?creatorSessionId=${result.creatorSessionId}&filename=${result.filename}&style=${result.style}`;
            } else {
                window.location = "/500.html";
            }
        } else if (photo.getAttribute("src") != null) {
            // Show Loading Effect
            loadingEffect.style.display = "block";

            // Image from photo taking
            const res = await fetch(
                `${apiVersion}/creation/${styleName}/style/${styleName}/fileType/base64`,
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        image: photo.getAttribute("src"),
                    }),
                }
            );

            const result = await res.json();
            if (res.status === 200) {
                window.location = `/creation.html?creatorSessionId=${result.creatorSessionId}&filename=${result.filename}&style=${result.style}`;
            } else {
                window.location = "/500.html";
            }
        } else {
            // System Response
            systemResponseContent.innerText = "Please Select An Image!";
            systemResponse();
        }
    });
}
