let isLogin = false;
let hasSubs = false;
window.addEventListener("DOMContentLoaded", async (event) => {
    try {
        const searchparams = new URL(document.location).searchParams;
        await checkLogin();
        getSingleCreation(searchparams);
    } catch (err) {
        console.error(err.message);
    }
});

async function checkLogin() {
    const result = await checkUserLogin();
    isLogin = result.isUserLogin;
    if (result.hasSubs) {
        hasSubs = result.hasSubs;
    }
}

async function getSingleCreation(searchparams) {
    let creationImgBg = document.querySelector("#output-img-bg");
    let creationImg = document.querySelector("#output-img");

    // About Search Params
    let creatorSessionid = searchparams.get("creatorSessionId");
    let filename = searchparams.get("filename");
    let style = searchparams.get("style");
    // About Control Btns
    const downloadBtn = document.querySelector("#control-btn .download");
    const filterBtn = document.querySelector("#control-btn .filter");
    const backgroundBtn = document.querySelector("#control-btn .background");

    const res = await fetch(
        `${apiVersion}/creation/single/creatorsessionid/${creatorSessionid}/filename/${filename}/style/${style}`
    );
    const result = await res.json();
    if (res.status === 401) {
        window.location = "/";
    } else if (res.status === 200) {
        // About Transfer result image
        creationImg.innerHTML = `<img src="/output/${style}/${filename}">`;
        creationImgBg.innerHTML = `<img src="/output/${style}/${filename}">`;

        let creationImageToWatermark = document.querySelector(
            "#output-img img"
        );
        let creationImageBgToWatermark = document.querySelector(
            "#output-img-bg img"
        );

        // Force user login
        const triggerMenuBtn = document.querySelector("#triggerMenuBtn");
        // const triggerMenuBtnText = document.querySelector(
        //     "#triggerMenuBtn a.menu"
        // );
        const modal = document.getElementById("myModal");
        if (!result.isUserLogin) {
            let isFillingLoginForm = true;
            setTimeout(() => {
                document.querySelector("#triggerMenuBtn a.menu").click();
                document.querySelector("#navMenu #myBtn").click();

                window.onclick = function () {
                    modal.style.display = "block";
                };
                triggerMenuBtn.style.display = "none";
            }, 300);
        }

        // Make image and imageBg a canvas
        await makeImageCanvas(creationImageToWatermark, "JasonIsHandSome");
        await makeImageCanvas(creationImageBgToWatermark, "JasonIsHandSome");

        if (result.isUserLogin) {
            // revert userLogin Setting
            triggerMenuBtn.style.display = "flex";
            modal.style.display = "none";
            loginRegistermodal();

            // Login But NOT YET subscribe user: can only download image
            if (!hasSubs) {
                downloadBtn.style.display = "flex";
                document.querySelector("#upgrade-plan").style.display = "block";
            }
            // Login And Subscribed user: can download, filter and background
            if (hasSubs) {
                downloadBtn.style.display = "flex";
                filterBtn.style.display = "flex";
                backgroundBtn.style.display = "flex";
            }

            downloadBtn.addEventListener("click", downloadImage, false);
            backgroundBtn.addEventListener("click", changeBackground, false);
            filterBtn.addEventListener("click", filterImage, false);
        }
    } else {
        alert("Error");
    }
}

function downloadImage() {
    const downloadBtnATage = document.querySelector("#control-btn .download a");
    const canvas = document.querySelector("#canvas-original");
    canvas.toBlob(function (blob) {
        const newImg = document.createElement("img"),
            url = URL.createObjectURL(blob);
        newImg.onload = function () {
            // no longer need to read the blob so it's revoked
            URL.revokeObjectURL(url);
        };
        downloadBtnATage.setAttribute("href", URL.createObjectURL(blob));
        downloadBtnATage.setAttribute("download", "image");
    });
}

function makeImageCanvas(elemImage, text) {
    // Create test image to get proper dimensions of the image.
    var testImage = new Image();

    testImage.onload = function () {
        const h = testImage.height,
            w = testImage.width,
            img = new Image();

        // Once the image with the SVG of the watermark is loaded...
        img.onload = function () {
            // Make canvas with image and watermark
            const canvas = Object.assign(
                document.querySelector("#canvas-original"),
                {
                    width: w,
                    height: h,
                }
            );
            //Object.assign(document.createElement('canvas'), {width: w, height: h});
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, w, h);
            ctx.drawImage(testImage, 0, 0);
            ctx.drawImage(img, 0, 0);
            // If PNG can't be retrieved show the error in the console
            try {
                elemImage.src = canvas.toDataURL("image/png");
                //ctx.clearRect(0, 0, w, h);
            } catch (e) {
                console.error("Cannot watermark image with text:", {
                    src: elemImage.src,
                    text: text,
                    error: e,
                });
            }
        };
        // watermark image if NOT YET login/ subscription
        if (!hasSubs || !isLogin) {
            // SVG image watermark (HTML of text at bottom right)
            img.src =
                "data:image/svg+xml;base64," +
                window.btoa(
                    '<svg xmlns="http://www.w3.org/2000/svg" height="' +
                        h +
                        '" width="' +
                        w +
                        '">' +
                        '<foreignObject width="100%" height="100%">' +
                        '<div xmlns="http://www.w3.org/1999/xhtml">' +
                        '<div style="position: absolute;' +
                        "right: 50%;" +
                        "bottom: 50%;" +
                        "font-family: Tahoma;" +
                        "font-size: 50pt;" +
                        "background: #000;" +
                        "color: #fff;" +
                        "padding: 0.25em;" +
                        "border-radius: 0.25em;" +
                        "opacity: .6;" +
                        "margin: 0 0.125em 0.125em 0;" +
                        '">' +
                        text
                            .replace(/&/g, "&amp;")
                            .replace(/</g, "&lt;")
                            .replace(/>/g, "&gt;") +
                        "</div>" +
                        "</div>" +
                        "</foreignObject>" +
                        "</svg>"
                );
        } else {
            img.src =
                "data:image/svg+xml;base64," +
                window.btoa(
                    '<svg xmlns="http://www.w3.org/2000/svg" height="' +
                        h +
                        '" width="' +
                        w +
                        '">' +
                        '<foreignObject width="100%" height="100%">' +
                        '<div xmlns="http://www.w3.org/1999/xhtml">' +
                        '<div style="position: absolute;' +
                        "right: 0;" +
                        "bottom: 0;" +
                        "font-family: Tahoma;" +
                        "font-size: 20pt;" +
                        "background: #000;" +
                        "color: #fff;" +
                        "padding: 0.25em;" +
                        "border-radius: 0.25em;" +
                        "opacity: .6;" +
                        "margin: 0 0.125em 0.125em 0;" +
                        '">' +
                        "</div>" +
                        "</div>" +
                        "</foreignObject>" +
                        "</svg>"
                );
        }
    };
    testImage.src = elemImage.src;
}

/***********************************Filter Effect ***************************/
function filterImage() {
    document.querySelector("#filter-area").style.display = "block";
    const searchparams = new URL(document.location).searchParams;
    let filename = searchparams.get("filename");
    let style = searchparams.get("style");

    //const filterArea = document.querySelector("#filter-img");
    const previewArea = document.querySelector("#previewfilter-img");
    //filterArea.innerHTML = `<img src="/output/${style}/${filename}">`;
    previewArea.innerHTML = `<img src="/output/${style}/${filename}">`;

    // Confirm/ Cancel Filter
    const cancelFilterBtn = document.querySelector("#cancel-btn");
    const confirmFilterBtn = document.querySelector("#confirm-btn");
    cancelFilterBtn.addEventListener("click", () => {
        document.querySelector("#filter-area").style.display = "none";
    });
    confirmFilterBtn.addEventListener("click", async (e) => {
        e.preventDefault();

        const res = await fetch(
            `${apiVersion}/filter/filename/${filename}/style/${style}`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    image: previewArea.querySelector("img").getAttribute("src"),
                }),
            }
        );
        const result = await res.json();

        if (res.status === 200) {
            cancelFilterBtn.click();
            previewArea.innerHTML = ``;
            //display output
            location.reload();
        } else {
            alert("error");
        }
    });
}

let filterObj = {};

function updateFilter(name, value) {
    //document.querySelector("#filter-img img");
    const filterAreaImg = document.querySelector("#output-img-bg img");

    let filterCanvas = Object.assign(document.querySelector("#canvas-filter"), {
        width: filterAreaImg.width,
        height: filterAreaImg.height,
    });

    let ctxFilter = filterCanvas.getContext("2d");

    let filterArr = [];
    filterObj[name] = `${value}`;
    for (filter in filterObj) {
        filterArr.push(`${filter}(${filterObj[filter]})`);
    }

    ctxFilter.filter = filterArr.join(" ");

    ctxFilter.clearRect(0, 0, filterAreaImg.width, filterAreaImg.height);
    ctxFilter.drawImage(
        filterAreaImg,
        0,
        0,
        filterAreaImg.width * 1,
        filterAreaImg.height * 1
    );
    try {
        document.querySelector(
            "#previewfilter-img img"
        ).src = filterCanvas.toDataURL("image/png");
    } catch (e) {
        console.error("Cannot make filter preview image:", {
            src: filterAreaImg.src,
            error: e,
        });
    }
}

/***********************************Change Background Effect ***************************/
const uploadBgImageBtn = document.querySelector("button#uploadBgImageBtn"); // Preview change button (Upload background image)
const triggerFileUploadBtn = document.querySelector("#trigger-file-upload");
const fileUploadInput = document.querySelector("#bg");
// Define Loading Element
const loadEffect = document.querySelector(".bgchange-control #load-effect");

function changeBackground() {
    const bgModal = document.querySelector("#bg-change-area");
    bgModal.style.display = "block";
    document.querySelector("#preset-bg").style.display = "block";
    document.querySelector("#upload-bg").style.display = "block";
    document.querySelector(
        "#bg-change-area .creation-modal .decide-btn"
    ).style.bottom = "0";

    if (bgModal.querySelector("#preview")) {
        bgModal.querySelector("#preview").querySelector("img").remove();
    }
    document.querySelector("#bg-change-form").reset();

    // Set default preview image
    const searchparams = new URL(document.location).searchParams;
    let filename = searchparams.get("filename");
    let style = searchparams.get("style");
    const previewBgImg = document.querySelector("#previewbg-img");
    previewBgImg.innerHTML = `<img src="/output/${style}/${filename}" alt="image" />`;

    // Preview Selected Background Image
    const selectedBgImage = document.querySelector("#selected-bg");
    selectedBgImage.innerHTML = `<img />`;

    // Confirm/ Cancel Filter
    const cancelBgBtn = document.querySelector("#bg-cancel-btn");
    const confirmBgBtn = document.querySelector("#bg-confirm-btn");
    cancelBgBtn.addEventListener("click", () => {
        document.querySelector("#bg-change-area").style.display = "none";
        uploadBgImageBtn.style.display = "none";
        triggerFileUploadBtn.innerText = `UPLOAD BACKGROUND IMAGE`;
    });

    /****************** Select Preset Background************************/
    const triggerPresetImgBtn = document.querySelector("#trigger-preset-img");
    const presetImgArea = document.querySelector("#preset-img");
    triggerPresetImgBtn.addEventListener("click", () => {
        presetImgArea.style.display = "flex";

        document.querySelector("#bg-change-form").reset();
        selectedBgImage.innerHTML = `<img />`;
        uploadBgImageBtn.style.display = "none";
    });
    //
    const presetImages = document.querySelectorAll("#preset-img img");
    const presetBgImageBtn = document.querySelector("#presetBgImageBtn");
    for (let presetImage of presetImages) {
        presetImage.addEventListener("click", async () => {
            for (let a of presetImages) {
                a.classList.remove("active");
            }
            presetBgImageBtn.style.display = "block";
            presetImage.classList.add("active");
        });
    }

    // confirm to transfer
    presetBgImageBtn.addEventListener("click", async () => {
        const bgFilename = document.querySelector("#preset-img img.active")
            .dataset.id;
        // Trigger loading effect
        loadEffect.style.display = "block";
        cancelBgBtn.style.display = "none";

        const res = await fetch(
            `/api/v1/background?style=${style}&filename=${filename}`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    selectedBg: bgFilename,
                }),
            }
        );

        const result = await res.json();
        if (res.status === 200) {
            // Clear loading effect
            loadEffect.style.display = "none";
            cancelBgBtn.style.display = "flex";

            // Display preview Image
            const imgHTML = `<img src="/bgTransferOutput/${style}/${filename}" alt="image" />`;
            previewBgImg.innerHTML = imgHTML;
            confirmBgBtn.style.display = "flex";

            // Clear preview change btn
            //uploadBgImageBtn.style.display = "none";

            // Disable select/ upload background function
            document.querySelector("#preset-bg").style.display = "none";
            document.querySelector("#upload-bg").style.display = "none";
            document.querySelector(
                "#bg-change-area .creation-modal .decide-btn"
            ).style.bottom = "50%";
        }
    });

    /****************** Upload Own Background************************/
    // Trigger File Upload Buttons
    triggerFileUploadBtn.addEventListener("click", () => {
        presetImgArea.style.display = "none";
        presetBgImageBtn.style.display = "none";
        fileUploadInput.click();
    });
    // Upload background image and start transfer
    document
        .querySelector("#bg-change-form")
        .addEventListener("submit", async (e) => {
            try {
                e.preventDefault();
                // Trigger loading effect
                loadEffect.style.display = "block";
                cancelBgBtn.style.display = "none";

                const file = document.querySelector("#bg");
                // Image from file upload
                const formData = new FormData();
                formData.append("bg", file.files[0]);

                const res = await fetch(
                    `/api/v1/background?style=${style}&filename=${filename}`,
                    {
                        method: "POST",
                        body: formData,
                    }
                );

                // Clear loading effect
                loadEffect.style.display = "none";
                cancelBgBtn.style.display = "flex";

                // Display preview Image
                const imgHTML = `<img src="/bgTransferOutput/${style}/${filename}" alt="image" />`;
                previewBgImg.innerHTML = imgHTML;
                confirmBgBtn.style.display = "flex";

                // Clear selected image and preview change btn
                selectedBgImage.innerHTML = `<img />`;
                uploadBgImageBtn.style.display = "none";

                // Disable select/ upload background function
                document.querySelector("#preset-bg").style.display = "none";
                document.querySelector("#upload-bg").style.display = "none";
                document.querySelector(
                    "#bg-change-area .creation-modal .decide-btn"
                ).style.bottom = "50%";
            } catch (err) {
                window.location = "/500.html";
            }
        });

    // Confirm to replace existing image
    confirmBgBtn.addEventListener("click", async (e) => {
        try {
            e.preventDefault();

            //replace output with my bgTransferOutput
            const result = await fetch(`/api/v1/background/replace`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    style: style,
                    filename: filename,
                }),
            });

            const resultMsg = await result.json();

            if (resultMsg.message === "successful") {
                //display output
                location.reload();
                //hide modal
                bgModal.setAttribute("hidden", true);

                //reload the page
                location.reload();
            } else {
                alert("unable to save your photo, please try again");
            }
        } catch (err) {
            window.location = "/500.html";
        }
    });
}
// Preview Uploaded Image (trigger by html)
function previewBgImage(event) {
    const reader = new FileReader();
    reader.onload = function () {
        document
            .querySelector("#selected-bg img")
            .setAttribute("src", reader.result);
    };
    reader.readAsDataURL(event.target.files[0]);
    if (event.target.files[0]) {
        triggerFileUploadBtn.innerText = `SELECT ANOTHER BACKGROUND IMAGE`;
        uploadBgImageBtn.style.display = "block";
    }
}

/*****************************Carousel**********************/
// Data
let bgSliderOptions = [
    {
        id: 0,
        bgName: "ocean",
    },
    {
        id: 1,
        bgName: "beach",
    },
    {
        id: 2,
        bgName: "moon",
    },
];

const presetImgArea = document.querySelector("#preset-img");
for (let i = 0; i < bgSliderOptions.length; i++) {
    presetImgArea.innerHTML += `
        <img
            data-id="preset-${bgSliderOptions[i].id}.jpg"
            src="/bgInput/preset-${bgSliderOptions[i].id}.jpg"
            alt="${bgSliderOptions[i].bgName}"
            class="preset-img"
        />`;
}
