window.addEventListener("DOMContentLoaded", async (event) => {
    try {
        displayCreations(1);
    } catch (err) {
        console.error(err.message);
    }
});

let displayData = [];
let eachPageShowingItemsNum = 6;
const creationList = document.querySelector("#creation-list");
// Pagination
const pagination = document.querySelector(".cdp");
pagination.setAttribute("data-actpage", 1);
let currentPaginationPage = parseInt(pagination.getAttribute("data-actpage"));

function pageination() {
    let paginationItems = document.querySelectorAll(".cdp_i");

    for (let paginationItem of paginationItems) {
        paginationItem.addEventListener("click", (event) => {
            event.preventDefault();

            let goToPage = paginationItem
                .getAttribute("href")
                .replace("#!", "");
            if (goToPage === "+1") {
                // Next button
                currentPaginationPage++;
            } else if (goToPage === "-1") {
                // Previous button
                currentPaginationPage--;
            } else {
                currentPaginationPage = parseInt(goToPage);
            }
            pagination.setAttribute("data-actpage", currentPaginationPage);

            displayCreations(currentPaginationPage);
        });
    }
}

async function displayCreations(currentPaginationPage) {
    let createPaginationItems;
    // Clear display items
    creationList.innerHTML = ``;

    // calculate offset
    if (isNaN(parseInt(currentPaginationPage))) {
        return;
    }
    const fetchDataOffset =
        eachPageShowingItemsNum * (parseInt(currentPaginationPage) - 1);
    const fetchDataRes = await fetch(
        `${apiVersion}/creation/myCreations?offset=${fetchDataOffset}`
    );
    const fetchDataResult = await fetchDataRes.json();

    //If no creation, display no creation div
    if (fetchDataResult.creations.length === 0) {
        // delete pagination
        if (document.querySelector(".cdp_i")) {
            document.querySelector(".cdp_i").style.display = "none";
        }

        const noCreationDiv = document.querySelector(".no-creation");
        //Change padding of container
        const myCreationDiv = document.querySelector(".my-creations");
        myCreationDiv.style.display = "block";
        myCreationDiv.style.padding = "30px";
        noCreationDiv.removeAttribute("hidden");

        //When create btn is clicked, redirect to creation page
        // const createBtn = noCreationDiv.querySelector("button");
        // createBtn.addEventListener("click", (e) => {
        //     window.location = "/";
        // });
        return;
    }
    displayData = fetchDataResult.creations;
    const totalDataNum = fetchDataResult.creationsTotal[0]["count"];

    if (fetchDataRes.status === 200) {
        for (let i = 0; i < displayData.length; i++) {
            creationList.innerHTML += `
            <div class="creation-single">
                <a href=/creation.html?&filename=${
                    displayData[i].filename
                }&style=${displayData[i].style}>
                    <img src="/output/${displayData[i].style}/${
                displayData[i].filename
            }" />
                </a>
                <div class="creation-details">
                    <div>
                        <p>Birthday: ${moment(
                            displayData[i]["created_at"]
                        ).format("DD/MM/YYYY")}</p>
                        <p>Style: ${displayData[i].style}</p>
                    </div>
                    <i class="fas fa-trash" onclick="deleteCreation(${
                        displayData[i].id
                    })"></i>
                </div>
            </div>
            `;
        }

        if (totalDataNum > eachPageShowingItemsNum) {
            for (let i = 0; i < totalDataNum / eachPageShowingItemsNum; i++) {
                createPaginationItems += `
                <a href="#!${i + 1}" class="cdp_i">${i + 1}</a>`;
            }
            pagination.innerHTML = `
            <a href="#!-1" class="cdp_i prev">prev</a>
            ${createPaginationItems}
            <a href="#!+1" class="cdp_i next">next</a>
            `;
            pageination();

            // Set prev/ next btns disappear when on first/ last page
            const PagePrevBtn = document.querySelector(".cdp_i.prev");
            const PageNextBtn = document.querySelector(".cdp_i.next");
            const maxPageNum = Math.ceil(
                totalDataNum / eachPageShowingItemsNum
            );
            if (currentPaginationPage === 1) {
                PagePrevBtn.style.display = "none";
            }

            if (currentPaginationPage === maxPageNum) {
                PageNextBtn.style.display = "none";
            }
            return;
        }
        pagination.innerHTML = `<a href="#!1" class="cdp_i">1</a>`;
        pageination();

        // If there is no active sub, add watermark
        const subs = await fetch(`${apiVersion}/user/profile/subscription`);
        const subsData = await subs.json();
        const allImages = document.querySelectorAll(".creation-single img");
        if (subs.status === 401) {
            for (let allImage of allImages) {
                makeImageCanvas(allImage, "JasonIsHandSome");
            }
        }
    } else {
        console.log("error");
    }
}

async function deleteCreation(creationId) {
    await fetch(`${apiVersion}/creation/myCreations/delete`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            id: creationId,
        }),
    });

    systemResponse();
    displayCreations(currentPaginationPage);
}

function makeImageCanvas(elemImage, text) {
    // Create test image to get proper dimensions of the image.
    var testImage = new Image();

    testImage.onload = function () {
        const h = testImage.height,
            w = testImage.width,
            img = new Image();

        // Once the image with the SVG of the watermark is loaded...
        img.onload = function () {
            // Make canvas with image and watermark
            const canvas = Object.assign(document.querySelector("#canvas"), {
                width: w,
                height: h,
            });
            //Object.assign(document.createElement('canvas'), {width: w, height: h});
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, w, h);
            ctx.drawImage(testImage, 0, 0);
            ctx.drawImage(img, 0, 0);
            // If PNG can't be retrieved show the error in the console
            try {
                elemImage.src = canvas.toDataURL("image/png");
                //ctx.clearRect(0, 0, w, h);
            } catch (e) {
                console.error("Cannot watermark image with text:", {
                    src: elemImage.src,
                    text: text,
                    error: e,
                });
            }
        };

        // SVG image watermark (HTML of text at bottom right)
        img.src =
            "data:image/svg+xml;base64," +
            window.btoa(
                '<svg xmlns="http://www.w3.org/2000/svg" height="' +
                    h +
                    '" width="' +
                    w +
                    '">' +
                    '<foreignObject width="100%" height="100%">' +
                    '<div xmlns="http://www.w3.org/1999/xhtml">' +
                    '<div style="position: absolute;' +
                    "right: 50%;" +
                    "top: 50%;" +
                    "font-family: Tahoma;" +
                    "font-size: 70px;" +
                    "background: #000;" +
                    "color: #fff;" +
                    "padding: 0.25em;" +
                    "border-radius: 0.25em;" +
                    "opacity: .6;" +
                    "margin: 0 0.125em 0.125em 0;" +
                    '">' +
                    text
                        .replace(/&/g, "&amp;")
                        .replace(/</g, "&lt;")
                        .replace(/>/g, "&gt;") +
                    "</div>" +
                    "</div>" +
                    "</foreignObject>" +
                    "</svg>"
            );
    };
    testImage.src = elemImage.src;
}
