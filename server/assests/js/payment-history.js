const table = document.querySelector("tbody");

window.addEventListener("DOMContentLoaded", async () => {
    await updateHistory();
});

async function updateHistory() {
    const subs = await fetch(`${apiVersion}/user/profile/all/subscription`);
    const subsData = await subs.json();

    // If there is no payment record
    if (subsData.length === 0) {
        document.querySelector(".no-creation").removeAttribute("hidden");
        document.querySelector(".table-striped").style.display = "none";
        return;
    }
    for (let sub of subsData) {
        let {
            id,
            plan_start_date: issueDate,
            plan_name: planName,
            total_price: totalPrice,
        } = sub;

        issueDate = moment(new Date(issueDate)).format("L");
        planName = capitalizeFirstLetter(planName);

        const row = ` <tr>
            <th scope="row">${id}</th>
            <td>${issueDate}</td>
            <td>${planName} Subscription</td>
            <td>HKD ${totalPrice}</td>
            </tr>`;

        table.insertAdjacentHTML("beforeend", row);
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
