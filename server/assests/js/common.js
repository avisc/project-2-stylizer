let loginCallback = null;
let loginPlan = null;

const navMenu = document.querySelector("#navMenu");
const apiVersion = "/api/v1";

// About switching between login user and visitor
const myAccountDropDown = document.querySelector("#navMenu .menu-dropbtn");
const triggerModalBtn = document.querySelector("#navMenu #myBtn");
let isUserLogin = false;

// Nav Bar
const nav = document.querySelector("#navMenu");
const triggerMenuBtn = document.querySelector("#triggerMenuBtn");
const triggerMenuBtnText = document.querySelector("#triggerMenuBtn a.menu");
let isMenuOpen = false;

function main() {
    navBarDisplay();
    checkUserLogin();
    if (navMenu.querySelector('li[id="myBtn"]')) {
        loginRegistermodal();
        switchLoginAndRegister();
    }
}

main();

// System Response
const systemResponseContent = document.querySelector("#system-response");
function systemResponse() {
    systemResponseContent.classList.remove("fadeOut");
    systemResponseContent.classList.add("fadeIn");
    setTimeout(function () {
        systemResponseContent.classList.remove("fadeIn");
        systemResponseContent.classList.add("fadeOut");
    }, 5000);
}

// Check Login Status
async function checkUserLogin() {
    const res = await fetch(`${apiVersion}/user/`);
    const result = await res.json();
    if (res.status === 200) {
        isUserLogin = result.isUserLogin;
        // If user login
        if (isUserLogin) {
            //Check if user has subscription
            //QUESTION: why check?
            const hasSubs = await hasSubscription();

            //Hide login/register button(modal)
            if (triggerModalBtn) {
                triggerModalBtn.style.display = "none";
            }

            //Show my account dropdown
            myAccountDropDown.style.display = "block";

            //Show logout button
            const logoutBtn = navMenu.querySelector('a[onclick="logout()"]');
            logoutBtn.removeAttribute("hidden");

            return {
                isUserLogin: isUserLogin,
                hasSubs: hasSubs,
            };
        } else {
            triggerModalBtn.style.display = "block";
            myAccountDropDown.style.display = "none";

            return {
                isUserLogin: isUserLogin,
            };
        }
    }
}

// Sticky Menu Bar
window.onscroll = function () {
    stickyMenu();
};
const sticky = triggerMenuBtn.offsetTop;
function stickyMenu() {
    if (window.pageYOffset >= sticky) {
        triggerMenuBtn.classList.add("sticky");
    } else {
        triggerMenuBtn.classList.remove("sticky");
    }
}
function navBarDisplay() {
    const modal = document.getElementById("myModal");
    triggerMenuBtnText.addEventListener("click", () => {
        if (!isMenuOpen) {
            nav.style.transform = "translateY(0)";
            triggerMenuBtnText.innerText = "CLOSE";
            triggerMenuBtnText.style.color = "#fff";
        } else {
            nav.style.transform = "translateY(-100%)";
            triggerMenuBtnText.innerText = "MENU";
            //Hide login form
            if (modal) {
                modal.click();
            }
        }
        isMenuOpen = !isMenuOpen;
    });

    const menuDropDownBtn = document.querySelector(".menu-dropbtn");
    const menuDropDownContent = document.querySelector(
        ".menu-dropdown-content"
    );
    let isOpenDropdownMenu = false;
    menuDropDownBtn.addEventListener("click", () => {
        if (!isOpenDropdownMenu) {
            menuDropDownContent.style.display = "flex";
        } else {
            menuDropDownContent.style.display = "none";
        }

        isOpenDropdownMenu = !isOpenDropdownMenu;
    });
}

let isFillingLoginForm = true;

//Register/ Login Modal
function loginRegistermodal() {
    const modal = document.getElementById("myModal");
    // When the user clicks the button, open the modal
    triggerModalBtn.addEventListener("click", () => {
        modal.style.display = "block";
    });

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        const switchBtn = document.getElementById("switch-login-register");
        if (event.target == modal) {
            modal.style.display = "none";
            isFillingLoginForm = false;
            switchBtn.click();

            //Reset all form
            const loginForm = document.getElementById("login-form");
            const registerForm = document.getElementById("register-form");

            loginForm.reset();
            registerForm.reset();
        }
    };
}

function switchLoginAndRegister() {
    const formTitle = document.querySelector("#modal-right h3");
    const switchBtn = document.getElementById("switch-login-register");
    const loginForm = document.getElementById("login-form");
    const registerForm = document.getElementById("register-form");

    switchBtn.addEventListener("click", (event) => {
        event.preventDefault();
        if (!isFillingLoginForm) {
            registerForm.reset();
            formTitle.innerText = "Login";
            registerForm.style.display = "none";
            loginForm.style.display = "block";
            switchBtn.innerText = "Free Register!";
        } else {
            loginForm.reset();
            formTitle.innerText = "Register";
            registerForm.style.display = "block";
            loginForm.style.display = "none";
            switchBtn.innerText = "Already Registered?";
        }

        isFillingLoginForm = !isFillingLoginForm;
    });
}

function login(callback = null, plan = null) {
    const loginForm = document.querySelector("form[id=login-form]");
    //When login form is submitted
    loginForm.addEventListener("submit", async (e) => {
        try {
            e.preventDefault();
            //Get form info
            const form = e.target;
            const email = form.email.value;
            const password = form.password.value;

            //Send login request
            const login = await fetch(`${apiVersion}/user/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                }),
            });

            const loginResult = await login.json();

            //If email/password is incorrect
            if (login.status === 401) {
                // System Response
                systemResponseContent.innerText = loginResult.message;
                systemResponse();
                return;
            }

            //Reset form
            form.reset();

            if (callback !== null) {
                await callback(plan);
            }
        } catch (err) {
            console.log(err);
        }
    });
}

const loginForm = document.querySelector("form[id=login-form]");
//When login form is submitted
if (loginForm) {
    loginForm.addEventListener("submit", async (e) => {
        try {
            e.preventDefault();
            //Get form info
            const form = e.target;
            const email = form.email.value;
            const password = form.password.value;

            //Send login request
            const login = await fetch(`${apiVersion}/user/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                }),
            });

            const loginResult = await login.json();

            //If email/password is incorrect
            if (login.status === 401) {
                // System Response
                systemResponseContent.innerText = loginResult.message;
                systemResponse();
                return;
            }

            //Reset form
            form.reset();

            //Trigger callback function
            if (loginCallback !== null) {
                const hasSubs = await hasSubscription();

                if (hasSubs === true) {
                    // System Response
                    systemResponseContent.innerText = "Already subscribed!";
                    systemResponse();
                    return;
                }

                await loginCallback(loginPlan);
            } else {
                const modal = document.getElementById("myModal");
                modal.click();
                triggerMenuBtn.click();

                // System Response
                systemResponseContent.innerHTML = `<p>Signed In successfully</p>`;
                systemResponse();

                checkUserLogin();
                // If this is creation result page
                if (window.location.pathname === "/creation.html") {
                    const searchparams = new URL(document.location)
                        .searchParams;
                    await checkLogin();
                    getSingleCreation(searchparams);
                }
            }
        } catch (err) {
            console.log(err);
        }
    });
}

//Pass in function for redirection to checkout page
function loginInfo(callback, plan) {
    loginCallback = callback;
    loginPlan = plan;
}

function showLogin() {
    const btn = document.getElementById("myBtn");
    nav.style.transform = "translateY(0)";
    triggerMenuBtnText.innerText = "CLOSE";
    triggerMenuBtnText.style.color = "#fff";
    btn.click();
    isMenuOpen = true;
}

async function hasSubscription() {
    //Check if already have subscription
    const subs = await fetch(`${apiVersion}/user/profile/hasSubscription`);
    const hasSubs = (await subs.json()).hasSubscription;

    //Hide subscription button if have subs
    if (hasSubs) {
        navMenu.querySelector('a[href="/subscription.html"]').style.display =
            "none";
    }
    return hasSubs;
}

//Register function
const register = document.getElementById("register-form");
//When register form is submmited
if (register) {
    register.addEventListener("submit", async (event) => {
        try {
            event.preventDefault();

            //Get form info
            const form = event.target;
            const email = form.email.value;
            const password = form.password1.value;
            const confirmPassword = form.password2.value;

            const searchparams = window.location.search;

            //Send signup request
            const signup = await fetch(`${apiVersion}/register/signup`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                    confirmationPassword: confirmPassword,
                    searchparams: searchparams,
                }),
            });

            const registerResult = await signup.json();

            form.reset();

            // System Response
            systemResponseContent.innerText = registerResult.message;
            systemResponse();
            return;
        } catch (err) {
            console.log(err);
            alert(err.message);
        }
    });
}

//Logout
const profileLeft = document.querySelector("#profile-left");
if (profileLeft) {
    const logoutDiv = profileLeft.lastElementChild;
    logoutDiv.querySelector("p").addEventListener("click", logout);
}

async function logout() {
    try {
        await fetch(`${apiVersion}/user/logout`);

        window.location = "/";

        // System Response
        systemResponseContent.innerText = "Signed out successfully";
        systemResponse();
    } catch (err) {
        console.error(err);
        // System Response
        systemResponseContent.innerText = "Unable to logout, please try again!";
        systemResponse();
    }
}
