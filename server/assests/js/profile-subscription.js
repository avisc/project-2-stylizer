window.addEventListener("DOMContentLoaded", async () => {
    await getSubscription();
    subs.removeAttribute("hidden");
    await cancelPlan();
});

const subs = document.querySelector("#subscription-details");
// subs.style.display = "initial";

const detailsField = document.querySelector(".detail");
const planField = detailsField.querySelector("h5 span");
const startDateField = detailsField.querySelector("p span");
const endDateField = detailsField.querySelector("p:nth-child(3) span");
const subsDetail = document.querySelector("#subscription-details div");
const cancelBtn = document.querySelector("#cancel-plan");
const cross = "fas fa-times-circle";
const tick = "fas fa-check-circle";

async function getSubscription() {
    const subsDiv = document.querySelectorAll("#subscription-details div");
    //check if the user has active subs
    const subs = await fetch(`${apiVersion}/user/profile/subscription`);
    const subsData = await subs.json();

    //if not, display a button to redirect to subs page
    if (subs.status === 401) {
        //Remove content inside
        for (let div of subsDiv) {
            div.remove();
        }
        //Create new div
        const newDiv = document.createElement("div");
        const html = "<h5>No Subscription</h5>";
        const detail = document.querySelector("#subscription-details");
        const joinBtnHtml = "<button class='join'>Join Now!</button>";
        detail.insertAdjacentHTML("beforeend", html);
        detail.insertAdjacentHTML("afterend", joinBtnHtml);

        //Redirect when button is clicked
        const joinBtn = document.querySelector("#profile-right .join");
        joinBtn.addEventListener("click", () => {
            window.location = "/subscription.html";
        });

        return;
    }

    //if has, display the information (monthly/yearly)(duration)
    subsDetail.insertAdjacentHTML("afterbegin", ` <i class=${tick}></i>`);
    const {
        plan_name: planName,
        plan_start_date: startDate,
        plan_end_date: endDate,
        is_cancelled: isCancelled,
        is_plan_active: isPlanActive,
    } = subsData;

    //Check if the plan is cancelled or not, and display accordingly
    planField.innerHTML = planName;
    if (isCancelled === true) {
        subsDetail.querySelector("h5").innerHTML = "Cancelled";
        subsDetail.insertAdjacentHTML("afterbegin", ` <i class=${cross}></i>`);
        document
            .querySelector(".detail")
            .insertAdjacentHTML(
                "beforeend",
                "<p>You can continue to use until the end date</p>"
            );
    }
    cancelBtn.removeAttribute("hidden");
    startDateField.innerHTML = moment(new Date(startDate)).format("L");
    endDateField.innerHTML = moment(new Date(endDate)).format("L");
}

async function cancelPlan() {
    const cancelBtn = document.querySelector("#cancel-plan");
    cancelBtn.addEventListener("click", async (e) => {
        try {
            e.preventDefault();

            //cancel subs
            await fetch(`${apiVersion}/user/profile/cancelSubscription`, {
                method: "DELETE",
            });

            //change html
            subsDetail.querySelector("h5").innerHTML = "Cancelled";
            subsDetail.insertAdjacentHTML(
                "afterbegin",
                ` <i class=${cross}></i>`
            );
            document
                .querySelector(".detail")
                .insertAdjacentHTML(
                    "beforeend",
                    "<p>You can continue to use until the end date</p>"
                );

            //Can reactive when cancel_at_period_end = false
        } catch (err) {
            console.error(err);
        }
    });
}
