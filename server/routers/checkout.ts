import express from "express";
import { checkoutController } from "../main";

export const checkoutRoutes = express.Router();

checkoutRoutes.post("/", checkoutController.post);
checkoutRoutes.get("/key", checkoutController.getKey);
checkoutRoutes.get("/success", checkoutController.onSuccess);
checkoutRoutes.post("/recurSubscription", checkoutController.recurSubscription);
