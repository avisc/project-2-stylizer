import express from 'express';
import {signupController} from '../main';

export const signupRoutes = express.Router();

signupRoutes.post("/signup",signupController.userSignup);
signupRoutes.get("/email-activate/token/:token", signupController.activateAccount);
