import express, { Request, Response } from "express";
import fetch from "node-fetch";
import { apiVersion } from "../utils/variables";
import bodyParser from "body-parser";
// import { stripe } from "../main";
export const webhookRoutes = express.Router();

//1. Get event from Stripe
//2. Post it to Route that handle new subs event

webhookRoutes.post(
    "/",
    bodyParser.raw({ type: "application/json" }),
    async (req: Request, res: Response) => {
        const data = req.body.data;
        let event = req.body;
        const eventType = req.body.type;

        // //TODO: change to .env secret when deploy
        // const endpointSecret = "whsec_MzA9hNqwRMM1rd8lXf6Gt3kSP4brMiJw";

        // if (endpointSecret) {
        //     const sig = req.headers["stripe-signature"];

        //     console.log(sig);

        //     try {
        //         event = stripe.webhooks.constructEvent(
        //             req.body,
        //             sig as string,
        //             endpointSecret
        //         );

        //         console.log(event);
        //     } catch (err) {
        //         res.status(400).send(`Webhook Error: ${err.message}`);
        //         return;
        //     }
        // }

        //Handle subscription update
        if (eventType === "customer.subscription.updated") {
            //check if it is an cancel sub event
            if (event.data.object.cancel_at_period_end === false) {
                const {
                    current_period_start,
                    current_period_end,
                    customer,
                    plan,
                } = data.object;

                await fetch(`${apiVersion}/checkout/recurSubscription`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        planName: plan.interval,
                        customerId: customer,
                        startDate: current_period_start,
                        endDate: current_period_end,
                    }),
                });
            }
        } else if (eventType === "customer.subscription.deleted") {
            const customerId = event.data.object.customer;
            await fetch(`${apiVersion}/user/inactiveSubs`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    customerId: customerId,
                }),
            });
        }

        res.sendStatus(200);
    }
);
