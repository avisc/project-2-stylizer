import express from "express";
import { backgroundController, bgUpload } from "../main";

export const backgroundRoutes = express.Router();

backgroundRoutes.post("/replace", backgroundController.replaceImage);

backgroundRoutes.post(
    "/",
    bgUpload.single("bg"),
    backgroundController.sendImage
);
