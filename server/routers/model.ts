export interface User {
    id: number;
    email: string;
    password: string;
    is_user_active: boolean;
}

export interface subscriptions {
    id?: number;
    user_id: number;
    stripe_session_id?: string;
    stripe_customer_id: string;
    current_plan_id: number;
    plan_start_date: Date;
    plan_end_date: Date;
    total_price: number;
    is_cancelled?: boolean;
    is_plan_active?: boolean;
}

export interface plans {
    id: number;
    plan_name: string;
    current_price: number;
}

export interface Creations {
    id: number;
    user_id: number;
    filename: string;
    is_file_active: boolean;
}

export interface recursivePayment {
    planName: string;
    customerId: number;
    startDate: string;
    endDate: string;
}
