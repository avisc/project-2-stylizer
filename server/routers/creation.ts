import express from "express";
import {
    creationController,
    africaUpload,
    aquarelleUpload,
    bangoUpload,
    chineseStyleUpload,
    hampsonUpload,
    laMuseUpload,
    rainPrincessUpload,
    theScreamUpload,
    theShipwreckOfTheMinotaurUpload,
    udnieUpload,
    waveUpload,
} from "../main";

export const creationRoutes = express.Router();
export const creationFilterRoutes = express.Router();

// Filter Creations
creationFilterRoutes.post(
    "/filename/:filename/style/:style",
    creationController.filterCreation
);

// Creations
creationRoutes.get(
    "/single/creatorsessionid/:creatorsessionid/filename/:filename/style/:style",
    creationController.getSingleCreation
);
creationRoutes.get("/myCreations/", creationController.getMyCreations);

//Delete creation
creationRoutes.delete("/myCreations/delete", creationController.deleteCreation);

//New creation through style transfer
creationRoutes.post(
    "/africa/style/:style/fileType/:fileType",
    africaUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/aquarelle/style/:style/fileType/:fileType",
    aquarelleUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/bango/style/:style/fileType/:fileType",
    bangoUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/chinese_style/style/:style/fileType/:fileType",
    chineseStyleUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/hampson/style/:style/fileType/:fileType",
    hampsonUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/la_muse/style/:style/fileType/:fileType",
    laMuseUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/rain_princess/style/:style/fileType/:fileType",
    rainPrincessUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/the_scream/style/:style/fileType/:fileType",
    theScreamUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/the_shipwreck_of_the_minotaur/style/:style/fileType/:fileType",
    theShipwreckOfTheMinotaurUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/udnie/style/:style/fileType/:fileType",
    udnieUpload.single("image"),
    creationController.addCreation
);
creationRoutes.post(
    "/wave/style/:style/fileType/:fileType",
    waveUpload.single("image"),
    creationController.addCreation
);
