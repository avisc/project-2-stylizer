import express from "express";
import { profileController } from "../main";

export const profileRoutes = express.Router();

profileRoutes.get("/hasSubscription", profileController.hasSubscription);
profileRoutes.get("/subscription", profileController.getSubscription);
profileRoutes.get("/all/subscription", profileController.getAllSubscriptions);
profileRoutes.delete(
    "/cancelSubscription",
    profileController.cancelSubscription
);
// Edit Profile - change password
profileRoutes.put("/edit", profileController.editProfile);
profileRoutes.get("/", profileController.getUserInfo);
