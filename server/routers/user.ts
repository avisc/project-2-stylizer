import express from "express";
import { userController } from "../main";

export const userRoutes = express.Router();

userRoutes.get("/", userController.checkLoginStatus);
userRoutes.get("/login/google", userController.loginGoogle);
userRoutes.get("/login/facebook", userController.loginFacebook);
userRoutes.post("/login", userController.login);
userRoutes.get("/logout", userController.logout);
userRoutes.put("/inactiveSubs", userController.inactiveSubs);
// userRoutes.get("/hasSubscription", userController.hasSubscription);
// userRoutes.get("/profile/subscription", userController.getSubscription);
// userRoutes.get("/profile/all/subscription", userController.getAllSubscriptions);
// userRoutes.delete(
//     "/profile/cancelSubscription",
//     userController.cancelSubscription
// );

// // Edit Profile - change password
// userRoutes.put("/profile/edit", userController.editProfile);
// userRoutes.get("/profile", userController.getUserInfo);
