import Knex from "knex";
import { table } from "../utils/tables";
import { plans, subscriptions } from "../routers/model";

export class CheckoutService {
    constructor(private knex: Knex) {}

    //QUESTION:What type should i use?
    getPlan = async (plan: string): Promise<any> => {
        if (plan === "month") {
            const plan = (
                await this.knex<plans>(table.PLANS).where(
                    "plan_name",
                    "monthly"
                )
            )[0];

            return { planId: plan.id, price: plan.current_price };
        } else if (plan === "year") {
            const plan = (
                await this.knex<plans>(table.PLANS).where("plan_name", "yearly")
            )[0];

            return { planId: plan.id, price: plan.current_price };
        }
    };

    //Create new Subscription
    createSubs = async (data: subscriptions) => {
        try {
            const result = await this.knex<subscriptions>(
                table.SUBSCRIPTIONS
            ).insert(data);

            return result;
        } catch (err) {
            console.error(err);
            return;
        }
    };

    //Get user current subs
    currentSub = async (customerId: number) => {
        //Get User current plan
        const userSub = (
            await this.knex(table.SUBSCRIPTIONS).where({
                stripe_customer_id: customerId,
                is_plan_active: true,
            })
        )[0];

        //Return user Id and current subs id
        return { userId: userSub.user_id, currentPlanId: userSub.id };
    };

    //Change current plan to inactive
    inactivePlan = async (currentPlanId: number) => {
        console.log(currentPlanId);

        await this.knex(table.SUBSCRIPTIONS)
            .where("id", currentPlanId)
            .update("is_plan_active", "false");
    };
}
