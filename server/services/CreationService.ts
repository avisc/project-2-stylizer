import Knex from "knex";

export class CreationService {
    constructor(private knex: Knex) {}

    async addCreation(userID: number, filename: string, style: string) {
        console.log("style: ", style);
        await this.knex("creations").insert({
            user_id: userID,
            filename: filename,
            style: style,
            updated_at: new Date(),
        });
    }

    async getSingleCreation(filename: string) {
        const result = await this.knex("creations")
            .where("filename", filename)
            .first();
        return result;
    }

    async getMyCreations(userID: number, offset: number) {
        const result = await this.knex("creations")
            .where("user_id", userID)
            .orderBy("created_at", "desc")
            .limit(6)
            .offset(offset);
        return result;
    }

    async getMyCreationsTotalNum(userID: number) {
        const result = await this.knex("creations")
            .count("*")
            .where("user_id", userID);
        return result;
    }

    async deleteCreation(creationId: number) {
        const result = await this.knex("creations")
            .where({ id: creationId })
            .del();

        return result;
    }
}
