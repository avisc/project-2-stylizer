import Knex from "knex";
import { table } from "../utils/tables";
import { User } from "../routers/model";
import { hashPassword } from "../utils/hash";

export class ProfileService {
    constructor(private knex: Knex) {}

    //Check if the user has active subscription
    countUserSubs = async (userId: number) => {
        const subs = await this.knex(table.SUBSCRIPTIONS)
            .count("user_id")
            .where({ user_id: userId, is_plan_active: true });

        return subs[0];
    };

    //Get user active subscription
    getUserSubs = async (userId: number) => {
        const subs = await this.knex(table.SUBSCRIPTIONS)
            .select(
                `${table.SUBSCRIPTIONS}.id`,
                `${table.SUBSCRIPTIONS}.user_id`,
                `${table.SUBSCRIPTIONS}.stripe_session_id`,
                `${table.SUBSCRIPTIONS}.stripe_customer_id`,
                `${table.SUBSCRIPTIONS}.stripe_subscription_id`,
                `${table.SUBSCRIPTIONS}.plan_start_date`,
                `${table.SUBSCRIPTIONS}.plan_end_date`,
                `${table.PLANS}.plan_name`,
                `${table.SUBSCRIPTIONS}.total_price`,
                `${table.SUBSCRIPTIONS}.is_cancelled`,
                `${table.SUBSCRIPTIONS}.is_plan_active`
            )
            .from(table.SUBSCRIPTIONS)
            .innerJoin(
                table.PLANS,
                `${table.SUBSCRIPTIONS}.current_plan_id`,
                `${table.PLANS}.id`
            )
            .where({
                user_id: userId,
                is_plan_active: true,
            });

        return subs[0];
    };

    //Get user all subscriptions for history display
    getUserAllSubs = async (userId: number) => {
        const subs = await this.knex(table.SUBSCRIPTIONS)
            .select(
                `${table.SUBSCRIPTIONS}.id`,
                `${table.SUBSCRIPTIONS}.plan_start_date`,
                `${table.PLANS}.plan_name`,
                `${table.SUBSCRIPTIONS}.total_price`
            )
            .from(table.SUBSCRIPTIONS)
            .innerJoin(
                table.PLANS,
                `${table.SUBSCRIPTIONS}.current_plan_id`,
                "=",
                `${table.PLANS}.id`
            )
            .where({
                user_id: userId,
            });

        return subs;
    };

    //Set is cancelled = true
    cancelSub = async (subId: number) => {
        try {
            await this.knex(table.SUBSCRIPTIONS)
                .where("id", subId)
                .update({ is_cancelled: true });
        } catch (err) {
            console.error(err);
        }
    };

    async getUserById(userID: number) {
        const user = await this.knex<User>(table.USERS)
            .where({ id: userID })
            .first();

        return user;
    }

    editProfile = async (userID: number, password: string) => {
        const hashedNewPassword = await hashPassword(password);
        try {
            await this.knex(table.USERS)
                .where("id", userID)
                .update({ password: hashedNewPassword });
        } catch (err) {
            console.error(err);
        }
    };
}
