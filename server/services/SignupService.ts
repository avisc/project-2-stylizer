import Knex from "knex";
import { table } from "../utils/tables";
import { hashPassword } from "../utils/hash";

export class SignupService {
    constructor(private knex: Knex){}

    createNewUser = async (email: string, password:string) => {

        const hashedPassword = await hashPassword(password);
        const user = await this.knex(table.USERS)
            .insert({
                email:email,
                password:hashedPassword,
                is_user_active:true,
            })
            .returning("id")

            return user[0];
    }    
}