export const PLAN_DETAILS = Object.freeze({
    monthly: 20,
    yearly: 128,
});

export const domainURL = "https://jasonisnothandsome.tk";
export const apiVersion = "/api/v1";
