export const table = Object.freeze({
    USERS:"users",
    PLANS:"plans",
    SUBSCRIPTIONS:"subscriptions",
    CREATIONS:"creations",
})