import { Request, Response, NextFunction } from "express";

export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) {
    if (req.session?.["user"]) {
        next();
    } else {
        res.status(401).json({ message: "Unauthorized" });
    }
}

export function isLoggedInHTML(
    req: Request,
    res: Response,
    next: NextFunction
) {
    if (req.session?.["user"]) {
        next();
    } else {
        //TODO: Should show popup?
        res.redirect("/index.html");
    }
}
