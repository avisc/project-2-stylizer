import * as Knex from "knex";

const userTableName = "users";
const creationTableName = "creations";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.dropTable(creationTableName);
    await knex.schema.createTable(creationTableName, (table) => {
        table.increments();
        table.integer("user_id").unsigned().notNullable();
        table.foreign("user_id").references(`${userTableName}.id`);
        table.string("filename").notNullable();
        table.boolean("is_file_active").notNullable().defaultTo(true);
        table.timestamps(false, true);
    });

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(creationTableName);
    await knex.schema.createTable(creationTableName, (table) => {
        table.increments();
        table.integer("user_id").unsigned().notNullable().unique();
        table.foreign("user_id").references(`${userTableName}.id`);
        table.string("filename").notNullable();
        table.boolean("is_file_active").notNullable();
        table.timestamps(false, true);
    });

}

