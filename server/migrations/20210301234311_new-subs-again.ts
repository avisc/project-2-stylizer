import * as Knex from "knex";

const subscriptionTableName = "subscriptions";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(subscriptionTableName, function (table) {
        table.string("stripe_subscription_id");
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable(subscriptionTableName, function (table) {
        table.dropColumn("stripe_subscription_id");
    });
}
