import * as Knex from "knex";

//users
//plans
//subscriptions
//creations


const userTableName = "users";
const planTableName = "plans";
const subscriptionTableName = "subscriptions";
const creationTableName = "creations";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(userTableName,(table)=>{
        table.increments();
        table.string("email").notNullable().unique();
        table.string("password").notNullable();
        table.boolean("is_user_active").notNullable();
        table.timestamps(false,true);
    });

    await knex.schema.createTable(planTableName,(table)=>{
        table.increments();
        table.string("plan_name").notNullable()
        table.integer("current_price").notNullable().unsigned();
        table.timestamps(false,true);
    });

    await knex.schema.createTable(subscriptionTableName,(table)=>{
        table.increments();
        table.integer("user_id").unsigned().notNullable().unique();
        table
            .foreign("user_id")
            .references(`${userTableName}.id`)
        table.integer("current_plan_id").notNullable();
        table
            .foreign("current_plan_id")
            .references(`${planTableName}.id`);
        table.timestamp("plan_start_date").notNullable();
        table.timestamp("plan_end_date").notNullable();
        table.string("payment_method").notNullable();
        table.integer("total_price").notNullable().unsigned();
        table.boolean("is_cancelled").notNullable();
        table.boolean("is_plan_active").notNullable();
        table.timestamps(false,true);
    });

    await knex.schema.createTable(creationTableName,(table)=>{
        table.increments();
        table.integer("user_id").unsigned().notNullable().unique();
        table
            .foreign("user_id")
            .references(`${userTableName}.id`);
        table.string("filename").notNullable();
        table.boolean("is_file_active").notNullable();
        table.timestamps(false,true);
    });
    

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(creationTableName);
    await knex.schema.dropTable(subscriptionTableName);
    await knex.schema.dropTable(planTableName);
    await knex.schema.dropTable(userTableName); 
}

