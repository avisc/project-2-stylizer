import * as Knex from "knex";

const creationTableName = "creations";
export async function up(knex: Knex): Promise<void> {
    
    await knex.schema.alterTable(creationTableName, table => {
        table.string("style").notNullable();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable(creationTableName, function (table) {
        table.dropColumn("style");
    });

}

