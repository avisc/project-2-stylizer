import * as Knex from "knex";

const subscriptionTableName = "subscriptions";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(subscriptionTableName, function (table) {
        table.string("stripe_session_id").alter();
        table.dropColumn("payment_method");
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable(subscriptionTableName, function (table) {
        table.string("stripe_session_id").notNullable().alter();
        table.string("payment_method").notNullable();
    });
}
