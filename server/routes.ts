import express from "express";
import { userRoutes } from "./routers/user";
import { profileRoutes } from "./routers/profile";
import { checkoutRoutes } from "./routers/checkout";
import { creationRoutes, creationFilterRoutes } from "./routers/creation";
import { webhookRoutes } from "./routers/webhook";
import { isLoggedInAPI } from "./utils/guards";
import { signupRoutes } from "./routers/signup";
import { backgroundRoutes } from "./routers/background";

export const routes = express.Router();

routes.use("/user/profile", isLoggedInAPI, profileRoutes);
routes.use("/user", userRoutes);
routes.use("/checkout", isLoggedInAPI, checkoutRoutes);
routes.use("/register", signupRoutes);
routes.use("/creation", creationRoutes);
routes.use("/filter", isLoggedInAPI, creationFilterRoutes);
routes.use("/background", backgroundRoutes);
//Webhook route (doesn't run through api/v1.)
routes.use("/", webhookRoutes);
