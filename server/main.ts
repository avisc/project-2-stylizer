import express from "express";
import expressSession from "express-session";
import Knex from "knex";
import path from "path";
import Stripe from "stripe";
import multer from "multer";
import dotenv from "dotenv";
import grant from "grant";
import cors from "cors";

import { apiVersion } from "./utils/variables";
dotenv.config();

const knexConfig = require("./knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const app = express();
//not sure what 'cors' is
app.use(cors());
app.use(express.json({ limit: "10mb" }));
app.use(express.urlencoded({ limit: "10mb", extended: true }));

app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);

// Multer
class MulterStorage {
    styleStorage = function (style: string) {
        return multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, `${__dirname}/creations/input/${style}`);
            },
            filename: function (req, file, cb) {
                cb(
                    null,
                    `${file.fieldname}-${Date.now()}.${
                        file.mimetype.split("/")[1]
                    }`
                );
            },
        });
    };
    
    bgStorage = function () {
        return multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, `${__dirname}/creations/bgInput`);
            },
            filename: function (req, file, cb) {
                cb(
                    null,
                    `${file.fieldname}-${Date.now()}.${
                        file.mimetype.split("/")[1]
                    }`
                );
            },
        });
    };
}

const multerStorage = new MulterStorage();
const africaStorage = multerStorage.styleStorage("africa");
const aquarelleStorage = multerStorage.styleStorage("aquarelle");
const bangoStorage = multerStorage.styleStorage("bango");
const chineseStyleStorage = multerStorage.styleStorage("chinese_style");
const hampsonStorage = multerStorage.styleStorage("hampson");
const laMuseStorage = multerStorage.styleStorage("la_muse");
const rainPrincessStorage = multerStorage.styleStorage("rain_princess");
const theScreamStorage = multerStorage.styleStorage("the_scream");
const theShipwreckOfTheMinotaurStorage = multerStorage.styleStorage(
    "the_shipwreck_of_the_minotaur"
);
const udnieStorage = multerStorage.styleStorage("udnie");
const waveStorage = multerStorage.styleStorage("wave");

export const africaUpload = multer({ storage: africaStorage });
export const aquarelleUpload = multer({ storage: aquarelleStorage });
export const bangoUpload = multer({ storage: bangoStorage });
export const chineseStyleUpload = multer({ storage: chineseStyleStorage });
export const hampsonUpload = multer({ storage: hampsonStorage });
export const laMuseUpload = multer({ storage: laMuseStorage });
export const rainPrincessUpload = multer({ storage: rainPrincessStorage });
export const theScreamUpload = multer({ storage: theScreamStorage });
export const theShipwreckOfTheMinotaurUpload = multer({
    storage: theShipwreckOfTheMinotaurStorage,
});
export const udnieUpload = multer({ storage: udnieStorage });
export const waveUpload = multer({ storage: waveStorage });

const bgStorage = multerStorage.bgStorage();
export const bgUpload = multer({ storage: bgStorage });

// Payment
export const stripe = new Stripe(process.env.STRIPE_SECRET_KEY as string, {
    apiVersion: "2020-08-27",
});

//TODO: testing facebook
//Grant setting for google login
const grantExpress = grant.express({
    defaults: {
        origin: process.env.PRODUCTION_URL,
        transport: "session",
        state: true,
    },
    google: {
        key: process.env.GOOGLE_CLIENT_ID || "",
        secret: process.env.GOOGLE_CLIENT_SECRET || "",
        scope: ["profile", "email"],
        callback: `${apiVersion}/user/login/google`,
    },
    facebook: {
        key: process.env.FB_CLIENT_ID || "",
        secret: process.env.FB_CLIENT_SECRET || "",
        custom_params: {
            response_type: "token",
            redirect_uri:
                "https://jasonisnothandsome.tk/connect/facebook/callback",
        },
        scope: ["email"],
        callback: `${apiVersion}/user/login/facebook`,
    },
});

app.use(grantExpress as express.RequestHandler);

// Create Service and Controller
//User Controller & Service
import { UserService } from "./services/UserService";
import { UserController } from "./controllers/UserController";
const userService = new UserService(knex);
export const userController = new UserController(userService);

//Profile Controller & Service
import { ProfileService } from "./services/ProfileService";
import { ProfileController } from "./controllers/ProfileController";
const profileService = new ProfileService(knex);
export const profileController = new ProfileController(profileService);

//Checkout Controller & Service
import { CheckoutService } from "./services/CheckoutService";
import { CheckoutController } from "./controllers/CheckoutController";
const checkoutService = new CheckoutService(knex);
export const checkoutController = new CheckoutController(checkoutService);

//Background Controller
import { BackgroundController } from "./controllers/BackgroundController";
export const backgroundController = new BackgroundController();

//SignUp Controller & Service
import { SignupService } from "./services/SignupService";
import { SignupController } from "./controllers/SignupController";
const signupService = new SignupService(knex);
export const signupController = new SignupController(
    signupService,
    userService
);

//Creation Controller & Service
import { CreationService } from "./services/CreationService";
import { CreationController } from "./controllers/CreationController";
const creationService = new CreationService(knex);
export const creationController = new CreationController(creationService);

// API
import { routes } from "./routes";

//Stripe webhook
app.use("/webhook", routes);

// Set API Routing
const API_VERSION = process.env.API_VERSION ?? "/api/v1";
app.use(API_VERSION, routes);

// Set public folder could be seen
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "assests")));
app.use(express.static(path.join(__dirname, "creations")));
app.use(express.static(path.join(__dirname, "creations")));
// handle 404 image
app.use("/output", (req, res) => {
    res.sendFile(path.join(__dirname, "assests/img/broken-image.png"));
});

//Guard private folder
import { isLoggedInHTML } from "./utils/guards";
app.use(isLoggedInHTML, express.static(path.join(__dirname, "private")));

app.use((req, res) => {
    res.redirect("/404.html");
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`[info] listening to port: [${PORT}]`);
});
