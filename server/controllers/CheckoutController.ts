import { Request, Response } from "express";
import { stripe } from "../main";
import Stripe from "stripe";
import { domainURL, apiVersion } from "../utils/variables";
import { CheckoutService } from "../services/CheckoutService";
// import moment from "moment";
import { fromUnixTime } from "date-fns";

export class CheckoutController {
    constructor(private checkoutService: CheckoutService) {}

    //Get key for stripe
    getKey = (req: Request, res: Response) => {
        res.json({
            publishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
            monthlyPlanId: process.env.MONTHLY_PLAN_ID,
            yearlyPlanId: process.env.YEARLY_PLAN_ID,
        });
    };

    //Handle stripe checkout
    post = async (req: Request, res: Response) => {
        const { priceId } = req.body;

        try {
            const session = await stripe.checkout.sessions.create({
                mode: "subscription",
                payment_method_types: ["card"],
                line_items: [
                    {
                        price: priceId,
                        // For metered billing, do not pass quantity
                        quantity: 1,
                    },
                ],
                // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
                // the actual Session ID is returned in the query parameter when your customer
                // is redirected to the success page.
                success_url: `${domainURL}${apiVersion}/checkout/success?session_id={CHECKOUT_SESSION_ID}`,
                cancel_url: `${domainURL}/subscription.html`,
            });

            res.send({
                sessionId: session.id,
            });
        } catch (e) {
            res.status(400);
            res.send({
                error: {
                    message: e.message,
                },
            });
        }
    };

    //Redirect here when checkout succeeded
    onSuccess = async (req: Request, res: Response) => {
        try {
            //Get user id
            const userId = req.session["user"].id;

            //Get session id
            const { session_id } = req.query;
            const session = await stripe.checkout.sessions.retrieve(
                session_id as string
            );

            //Get Customer id
            const customerId = session.customer;

            //Get invoice
            const invoices = await stripe.invoices.list({
                customer: customerId as string,
            });

            //Get invoice obj
            const invoice = await stripe.invoices.retrieve(invoices.data[0].id);

            //Get subs id
            const subscriptionId = invoice.lines.data[0].subscription;

            //Get lineItem
            const lineItem: Stripe.Response<
                Stripe.ApiList<Stripe.LineItem>
            > = await stripe.checkout.sessions.listLineItems(
                session_id as string
            );

            //If have line Item
            if (
                lineItem.data[0].price?.recurring !== null &&
                lineItem.data[0].price !== null
            ) {
                //Get plan id and price from plans table
                const plan = lineItem.data[0].price.recurring.interval;
                const planData = await this.checkoutService.getPlan(plan);

                const { planId, price } = planData;

                //Get created time from line item
                //QUESTION: Time format?
                const startDateUnix =
                    invoices.data[0].lines.data[0].period.start;
                const endDateUnix = invoices.data[0].lines.data[0].period.end;
                // let startDate = moment.unix(createdAt).format();
                let startDate = fromUnixTime(startDateUnix);
                let endDate = fromUnixTime(endDateUnix);

                //Get payment type
                // const paymentType = session.payment_method_types[0];

                //Prepare insert data
                let insertData = {
                    user_id: userId,
                    stripe_session_id: session_id as string,
                    stripe_customer_id: customerId as string,
                    stripe_subscription_id: subscriptionId,
                    current_plan_id: planId,
                    total_price: price,
                    plan_start_date: startDate,
                    plan_end_date: endDate,
                };

                //Insert new subscription data
                await this.checkoutService.createSubs(insertData);

                res.redirect("/profile/edit-profile.html");
            }
        } catch (err) {
            res.status(500).json({ message: "Internal server error" });
        }
    };

    recurSubscription = async (req: Request, res: Response) => {
        try {
            //1. Get data from webhook stripe
            let { planName, customerId, startDate, endDate } = req.body;

            //2. Find user ID and current plan id
            const {
                userId,
                currentPlanId,
            } = await this.checkoutService.currentSub(customerId);

            //3. Find plan (price, plan ID)
            const { planId, price } = await this.checkoutService.getPlan(
                planName
            );

            //convert UNIX date to date
            startDate = fromUnixTime(startDate);
            endDate = fromUnixTime(endDate);

            const insertData = {
                user_id: userId,
                stripe_customer_id: customerId as string,
                current_plan_id: planId,
                total_price: price,
                plan_start_date: startDate,
                plan_end_date: endDate,
            };

            //4. Insert new sub
            await this.checkoutService.createSubs(insertData);

            //5. Change current plan to inactive
            await this.checkoutService.inactivePlan(currentPlanId);

            res.end();
        } catch (err) {
            res.status(500).json({ message: "Internal server error" });
        }
    };
}
