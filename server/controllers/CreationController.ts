// Post Raw Image --> Uploads folder
// Get Creation List
// Get Creation (Individual)
import { Request, Response } from "express";
import { CreationService } from "../services/CreationService";
import fs from "fs";
import path from "path";
import fetch from "node-fetch";
let filename: string;
let filePathtoDelete: string;
let filePathtoMove: string;
export class CreationController {
    constructor(private creationService: CreationService) {}

    addCreation = async (req: Request, res: Response) => {
        try {
            console.log("start transfer");
            const isUserLogin = req.session?.["user"];
            const { fileType, style } = req.params;

            let userId: number;

            // define python program variables
            const pythonUrl = "http://localhost:8000/transfer?";
            const pythonParams = new URLSearchParams({
                checkpoint_dir: style,
                in_path: `input/${style}`,
                out_path: "output",
            });

            // validation
            if (!req.file && fileType != "base64") {
                res.status(400).json({
                    message: "Please Upload Image",
                });
                return;
            }

            // if the type is upload file
            if (req.file) {
                filename = req.file.filename;
                console.log("file: ", filename);
                filePathtoDelete = path.join(
                    __dirname,
                    `../creations/input/${style}/${filename}`
                );
                filePathtoMove = path.join(
                    __dirname,
                    `../creations/imgInput/${style}/${filename}`
                );

                if (isUserLogin) {
                    userId = req.session?.["user"]["id"];
                    await this.creationService.addCreation(
                        userId,
                        filename,
                        style
                    );
                }
                console.log("start fetch ");
                // fetch python server to transfer image
                const fetchPythonResult = await fetch(pythonUrl + pythonParams);

                if (fetchPythonResult.ok) {
                    // res.status >= 200 && res.status < 300

                    console.log("success transfer!!!!!");
                    res.json({
                        message: "successfully transfer image",
                        creatorSessionId: req.sessionID,
                        filename: filename,
                        style: style,
                    });
                    return;
                } else {
                    throw fetchPythonResult.statusText;
                }
            }

            // if the type is taking photo
            if (fileType === "base64") {
                filename = `image-${Date.now()}.jpeg`;
                console.log("file: ", filename);
                filePathtoDelete = path.join(
                    __dirname,
                    `../creations/input/${style}/${filename}`
                );
                filePathtoMove = path.join(
                    __dirname,
                    `../creations/imgInput/${style}/${filename}`
                );

                const img = req.body.image;
                const data = img.replace(/^data:image\/\w+;base64,/, "");
                const buf = Buffer.from(data, "base64");
                fs.writeFileSync(
                    path.join(__dirname, `../creations/input/${style}/`) +
                        `${filename}`,
                    buf
                );

                if (isUserLogin) {
                    userId = req.session?.["user"]["id"];
                    this.creationService.addCreation(userId, filename, style);
                }

                // fetch python server to transfer image
                const fetchPythonResult = await fetch(pythonUrl + pythonParams);
                if (fetchPythonResult.ok) {
                    // res.status >= 200 && res.status < 300

                    res.json({
                        message: "successfully transfer image",
                        creatorSessionId: req.sessionID,
                        filename: filename,
                        style: style,
                    });
                } else {
                    throw fetchPythonResult.statusText;
                }
            }
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Internal Server Error" });
        } finally {
            //Move file to imgInput
            fs.copyFileSync(filePathtoDelete, filePathtoMove);
            // delete file no matter transfer success or not
            fs.unlinkSync(filePathtoDelete);
        }
    };

    getSingleCreation = async (req: Request, res: Response) => {
        try {
            const isUserLogin = req.session?.["user"];
            const { creatorsessionid, filename, style } = req.params;
            const visitorSessionId = req.sessionID;

            // If visitor doesn't login, check if user' sessionid matches visitor' sessionid
            if (!isUserLogin) {
                console.log("get single image (no user~~)");
                // if sessionid not match
                if (visitorSessionId != creatorsessionid) {
                    res.status(401).json({ message: "unauthorized user" });
                    return;
                }
                // if sessionid match
                res.json({
                    message: "succuessfully got single creation",
                    isUserLogin: isUserLogin,
                });
                return;
            }

            // If user login, compare userid with creation's userid
            console.log("get single image (user login in~~)");
            const userId = req.session?.["user"]["id"];
            const creationResult = await this.creationService.getSingleCreation(
                filename as string
            );

            // If creator login, save this output image into db
            if (
                typeof creationResult === "undefined" &&
                visitorSessionId === creatorsessionid
            ) {
                await this.creationService.addCreation(
                    userId,
                    filename as string,
                    style as string
                );
                res.json({
                    message: "succuessfully got single creation",
                    isUserLogin: isUserLogin,
                });
                return;
            }

            if (creationResult.user_id != userId) {
                res.status(401).json({ message: "unauthorized user" });
                return;
            }
            res.json({
                message: "succuessfully got single creation",
                isUserLogin: isUserLogin,
            });
        } catch (err) {
            res.status(500).json({ message: "Internal Server Error" });
        }
    };

    getMyCreations = async (req: Request, res: Response) => {
        try {
            const offset = req.query.offset as string;
            const offsetInt = parseInt(offset);
            const userId = req.session?.["user"]["id"];

            const creations = await this.creationService.getMyCreations(
                userId,
                offsetInt
            );
            const creationsTotal = await this.creationService.getMyCreationsTotalNum(
                userId
            );
            res.json({
                message: "succuessfully got creations",
                creations: creations,
                creationsTotal: creationsTotal,
            });
        } catch (err) {
            res.status(500).json({ message: "Internal Server Error" });
        }
    };

    filterCreation = (req: Request, res: Response) => {
        try {
            const { filename, style } = req.params;

            const img = req.body.image;
            const data = img.replace(/^data:image\/\w+;base64,/, "");
            const buf = Buffer.from(data, "base64");
            fs.writeFileSync(
                path.join(__dirname, `../creations/output/${style}/`) +
                    `${filename}`,
                buf
            );

            res.json({
                message: "succuessfully filtered",
            });
        } catch (err) {
            res.status(500).json({ message: "Internal Server Error" });
        }
    };

    deleteCreation = async (req: Request, res: Response) => {
        try {
            const creationId = req.body.id;
            await this.creationService.deleteCreation(creationId);

            res.json({ message: "successfully deleted" });
        } catch (err) {
            res.status(500).json({ message: "Internal Server Error" });
        }
    };
}
