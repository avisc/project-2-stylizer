import { Request, Response } from "express";
import { SignupService } from "../services/SignupService";
import { UserService } from "../services/UserService";
import { apiVersion } from "../utils/variables";
import jwt from "jsonwebtoken";
import mailgun from "mailgun-js";
import dotenv from "dotenv";
dotenv.config();

const DOMAIN = "jasonisnothandsome.tk";
const mg = mailgun({
    apiKey: process.env.MAILGUN_APIKEY as string,
    domain: DOMAIN,
});

export class SignupController {
    constructor(
        private signupService: SignupService,
        private userService: UserService
    ) {}

    userSignup = async (req: Request, res: Response) => {
        try {
            const {
                email,
                password,
                confirmationPassword,
                searchparams,
            } = req.body;

            const user = await this.userService.getUserByEmail(email);

            if (user) {
                res.status(400).json({
                    message: "User with this email already exists.",
                });
                return;
            }

            if (password !== confirmationPassword) {
                res.status(400).json({
                    message:
                        "Your password and confirmation password do not match.",
                });
                return;
            }

            const token = jwt.sign(
                { email, password },
                process.env.JWT_ACC_ACTIVATE as any,
                { expiresIn: "20m" }
            );

            let data;

            if (searchparams) {
                data = {
                    from: "noreply@jasonisnothandsome.tk",
                    to: email,
                    subject: "Account Activation Link From Jasonisnothandsome",
                    //text: 'Testing some Mailgun awesomness!'
                    html: `
                    <p>Dear user,</p>
    
                    <h2>Thank you for registration. Please click on given link to activate your account</h2>
                    <a href="${process.env.PRODUCTION_URL}${apiVersion}/register/email-activate/token/${token}${searchparams}">Please through this to activate your account</a>
                    
                    <p>STYLISER</p>
                    `,
                };
            } else {
                data = {
                    from: "noreply@jasonisnothandsome.tk",
                    to: email,
                    subject: "Account Activation Link From Jasonisnothandsome",
                    //text: 'Testing some Mailgun awesomness!'
                    html: `
                    <p>Dear user,</p>
    
                    <h2>Thank you for registration. Please click on given link to activate your account</h2>
                    <a href="${process.env.PRODUCTION_URL}${apiVersion}/register/email-activate/token/${token}">Please through this to activate your account</a>
                    
                    <p>STYLISER</p>
                    `,
                };
            }

            mg.messages().send(data, function (error, body) {
                if (error) {
                    return res.json({
                        message: error.message,
                    });
                }
                return res.json({
                    message:
                        "Email has been sent, kindly activate your account.",
                });
            });
        } catch (err) {
            res.status(400).json({ message: "internal server error" });
        }
    };

    activateAccount = async (req: Request, res: Response) => {
        try {
            const { token } = req.params;
            const queryIndex = req.originalUrl.indexOf("?");
            const query = req.originalUrl.substring(queryIndex);

            if (token) {
                jwt.verify(
                    token,
                    process.env.JWT_ACC_ACTIVATE as any,
                    async (err: any, decodeToken: any) => {
                        if (err) {
                            return res
                                .status(400)
                                .json({ error: "Incorrect or Expired link." });
                        }
                        const { email, password } = decodeToken;
                        const newUser = await this.signupService.createNewUser(
                            email,
                            password
                        );

                        if (!newUser) {
                            res.status(500).json({
                                message:
                                    "Error in signup while account activation. Please register again.",
                            });
                            return;
                        } else {
                            //Add user to our session
                            req.session["user"] = {
                                id: newUser,
                            };
                        }

                        if (query) {
                            res.redirect(`/creation.html${query}`);
                            return;
                        } else {
                            res.redirect("/");
                            return;
                        }
                    }
                );
            } else {
                res.json({ error: "Something went worng!!!" });
                return;
            }
        } catch (err) {
            res.status(400).json({ message: "internal server error" });
        }
    };
}
