import { Request, Response } from "express";
import { checkPassword } from "../utils/hash";
import { ProfileService } from "../services/ProfileService";
import { stripe } from "../main";

export class ProfileController {
    constructor(private profileService: ProfileService) {}

    hasSubscription = async (req: Request, res: Response) => {
        const userId = req.session["user"].id;

        const subs = await this.profileService.countUserSubs(userId);

        if (subs.count > 0) {
            res.json({ hasSubscription: true });
        } else {
            res.json({ hasSubscription: false });
        }
    };

    //Get Active subscription
    getSubscription = async (req: Request, res: Response) => {
        try {
            const userId = req.session["user"].id;

            const subs = await this.profileService.getUserSubs(userId);

            if (!subs) {
                res.status(401).json({ message: "No subscription found" });
                return;
            } else {
                res.json(subs);
                return;
            }
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };

    //Get all subscription
    getAllSubscriptions = async (req: Request, res: Response) => {
        try {
            const userId = req.session["user"].id;

            const subs = await this.profileService.getUserAllSubs(userId);

            if (!subs) {
                res.status(401).json({ message: "No subscription found" });
                return;
            } else {
                res.json(subs);
                return;
            }
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };

    //Change is cancelled = true in subscription
    cancelSubscription = async (req: Request, res: Response) => {
        try {
            const userId = req.session["user"].id;

            //Get active subscription
            const subs = await this.profileService.getUserSubs(userId);

            //Get active sub Id
            const subId = subs.stripe_subscription_id;

            //Cancel sub at the end of the period
            await stripe.subscriptions.update(subId, {
                cancel_at_period_end: true,
            });

            //cancel plan
            await this.profileService.cancelSub(subs.id);

            res.json({ message: "successfully cancelled" });
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };

    //Change password
    editProfile = async (req: Request, res: Response) => {
        try {
            //Get password input
            const { currentPassword, newPassword } = req.body;
            //Check if user is logged in
            //QUESTION: Still need to check in here?
            // if (!req.session?.["user"]) {
            //     res.status(401).json({ message: "unauthorized" });
            //     return;
            // }
            //Get user password in DB
            const userID = req.session["user"].id;
            const user = await this.profileService.getUserById(userID);
            const currentPasswordFromDb = user?.password;

            //Compare passwords
            if (currentPasswordFromDb) {
                const match = await checkPassword(
                    currentPassword,
                    currentPasswordFromDb
                );
                if (!match) {
                    res.status(400).json({
                        message: "current password is not correct",
                    });
                    return;
                }
            }

            //Update new password
            await this.profileService.editProfile(userID, newPassword);

            res.json({ message: "successfully edited profile" });
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };

    //Get user info
    getUserInfo = async (req: Request, res: Response) => {
        try {
            const userId = req.session["user"].id;

            const user = await this.profileService.getUserById(userId);

            res.json(user);
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };
}
