import { Request, Response } from "express";
import fetch from "node-fetch";
import fs from "fs";
import path from "path";
let filepathToDelete: string;

//Need to have folder in input, imgInput

export class BackgroundController {
    constructor() {}

    sendImage = async (req: Request, res: Response) => {
        try {
            const style = req.query.style;
            const fileName = req.query.filename;
            const imgPath = `../server/creations/imgInput/${style}/${fileName}`;
            let bgFilePath;
            let result; // fetch changing background route result
            filepathToDelete = path.join(
                __dirname,
                `../creations/bgOutput/${fileName}`
            );

            // Upload Own Background
            if (req.file) {
                bgFilePath = req.file.path;

                result = await fetch("http://localhost:8000/background", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        bgFilePath: bgFilePath,
                        imgPath: imgPath,
                    }),
                });
                if (result.ok) {
                    const pythonUrl = "http://localhost:8000/transfer?";
                    const pythonParams = new URLSearchParams({
                        checkpoint_dir: style as string,
                        in_path: "bgOutput",
                        out_path: "bgTransferOutput",
                    });

                    // fetch python server to transfer image
                    const fetchPythonResult = await fetch(
                        pythonUrl + pythonParams
                    );

                    if (fetchPythonResult.ok) {
                        res.json({ message: "successful" });

                        return;
                    } else {
                        throw fetchPythonResult.statusText;
                    }
                } else {
                    throw result.statusText;
                }
            }

            // Pick Preset Background
            const { selectedBg } = req.body;

            bgFilePath = path.join(
                __dirname,
                `../creations/bgInput/${selectedBg}`
            );
            result = await fetch("http://localhost:8000/background", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    bgFilePath: bgFilePath,
                    imgPath: imgPath,
                }),
            });

            //If background change is successful
            if (result.ok) {
                const pythonUrl = "http://localhost:8000/transfer?";
                const pythonParams = new URLSearchParams({
                    checkpoint_dir: style as string,
                    in_path: "bgOutput",
                    out_path: "bgTransferOutput",
                });

                // fetch python server to transfer image
                const fetchPythonResult = await fetch(pythonUrl + pythonParams);

                if (fetchPythonResult.ok) {
                    res.json({ message: "successful" });

                    return;
                } else {
                    throw fetchPythonResult.statusText;
                }
            } else {
                throw result.statusText;
            }
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: err.message });
        } finally {
            //del bgOutput image
            fs.unlinkSync(filepathToDelete);
        }
    };

    replaceImage = (req: Request, res: Response) => {
        const { style, filename } = req.body;

        const inputPath = path.join(
            __dirname,
            `../creations/bgTransferOutput/${style}/${filename}`
        );

        const outputPath = path.join(
            __dirname,
            `../creations/output/${style}/${filename}`
        );

        //Move file to output, overwritten existing file
        fs.copyFileSync(inputPath, outputPath);

        res.json({ message: "successful" });
    };
}
