import { Request, Response } from "express";
import { checkPassword } from "../utils/hash";
import { UserService } from "../services/UserService";
// import { stripe } from "../main";
import fetch from "node-fetch";

export class UserController {
    constructor(private userService: UserService) {}

    checkLoginStatus = async (req: Request, res: Response) => {
        try {
            if (req.session?.["user"]) {
                res.json({
                    isUserLogin: true,
                });
                return;
            }
            res.json({
                isUserLogin: false,
            });
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    login = async (req: Request, res: Response) => {
        try {
            const { email, password } = req.body;

            const user = await this.userService.getUserByEmail(email);
            if (!user) {
                res.status(401).json({
                    message: "email/password incorrect",
                });
                return;
            }

            if (await checkPassword(password, user.password)) {
                req.session["user"] = {
                    id: user.id,
                };
            } else {
                res.status(401).json({
                    message: "email/password incorrect",
                });
                return;
            }

            res.json({
                message: "login successful",
            });
        } catch (err) {
            res.status(500).json({ message: "internal server error" });
        }
    };

    loginGoogle = async (req: Request, res: Response) => {
        try {
            //Receive access token
            const accessToken = req.session?.["grant"].response.access_token;
            //Fetching user information
            const fetchRes = await fetch(
                "https://www.googleapis.com/oauth2/v2/userinfo",
                {
                    method: "get",
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                }
            );
            const result = await fetchRes.json();

            //Check if email exist in our DB
            const user = await this.userService.getUserByEmail(result.email);

            //If user not found, register them
            if (!user) {
                const newUserId = await this.userService.registerWithSocialMed(
                    result.email
                );

                if (!newUserId) {
                    res.status(500).json({
                        message:
                            "Internal server error. Please register again.",
                    });
                    return;
                }

                //Add user id in our session
                if (req.session) {
                    req.session["user"] = {
                        id: newUserId,
                    };
                }

                //QUESTION: Can redirect to previous page?
                return res.redirect("/");
            }

            //Add user id in our session
            if (req.session) {
                req.session["user"] = {
                    id: user.id,
                };
            }

            //QUESTION: Can redirect to previous page?
            return res.redirect("/");

            return;
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };

    loginFacebook = async (req: Request, res: Response) => {
        try {
            const fetchRes = await fetch(
                "https://graph.facebook.com/v10.0/oauth/access_token?",
                {
                    method: "GET",
                    headers: {
                        client_id: "135587451640096",
                        client_secret: "557d8196ceab839ce4df858e3ee6edec",
                        redirect_uri:
                            "https://jasonisnothandsome.tk/api/v1/user/login/facebook",
                        code: "",
                    },
                }
            );
            const result = await fetchRes.json();

            //Check if email exist in our DB
            const user = await this.userService.getUserByEmail(result.email);

            //If user not found, register them
            if (!user) {
                const newUserId = await this.userService.registerWithSocialMed(
                    result.email
                );

                //Add user id in our session
                if (req.session) {
                    req.session["user"] = {
                        id: newUserId,
                    };
                }

                //QUESTION: Can redirect to previous page?
                return res.redirect("/");
            }

            if (req.session) {
                req.session["user"] = {
                    id: user.id,
                };
            }
            return res.redirect("/");
        } catch (err) {
            res.status(500).json({ message: "Internal Server Error" });
        }
    };

    logout = (req: Request, res: Response) => {
        try {
            //Remove user session
            if (req.session) {
                req.session.destroy((err) => {
                    if (err) {
                        res.status(400).send("Unable to logout");
                    } else {
                        res.json({ message: "logout successful" });
                    }
                });
            } else {
                res.status(401).json({ message: "you are not logged in" });
            }
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };

    //Change is active = true in subscription
    inactiveSubs = async (req: Request, res: Response) => {
        try {
            const { customerId } = req.body;

            //Get active subscription
            await this.userService.inactiveSub(customerId);

            res.json({ message: "successfully inactivate" });
        } catch (err) {
            res.status(500).json({ message: "Internal server error.⛔️" });
        }
    };
}
