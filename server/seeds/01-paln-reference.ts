import * as Knex from "knex";
import { table } from "../utils/tables";
import { PLAN_DETAILS } from "../utils/variables";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(table.CREATIONS).del();
    await knex(table.SUBSCRIPTIONS).del();
    await knex(table.PLANS).del();
    await knex(table.USERS).del();


    // Inserts seed entries
    const planKeys = Object.keys(PLAN_DETAILS);
    await knex(table.PLANS).insert(
        planKeys.map((key) => ({
            current_price: PLAN_DETAILS[key],
            plan_name: key,
        }))
    );
}
