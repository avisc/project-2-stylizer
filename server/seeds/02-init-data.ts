import * as Knex from "knex";
import { table } from "../utils/tables";
import { hashPassword } from "../utils/hash";

export async function seed(knex: Knex): Promise<void> {
    // // Deletes ALL existing entries
    // await knex("table_name").del();

    // // Inserts seed entries
    // await knex("table_name").insert([
    //     { id: 1, colName: "rowValue1" },
    //     { id: 2, colName: "rowValue2" },
    //     { id: 3, colName: "rowValue3" }
    // ]);

    const users = [
        { email: "avis@tecky.io", password: "1234", is_user_active: true },
        { email: "gigi@tecky.io", password: "1234", is_user_active: true },
        { email: "victor@tecky.io", password: "1234", is_user_active: true },
    ];

    for (const user of users) {
        user.password = await hashPassword(user.password);
    }

    // const [avisID, gigiID, victorID] = await knex(table.USERS)
    //     .insert(users)
    //     .returning("id");

    const [avisID, gigiID] = await knex(table.USERS)
        .insert(users)
        .returning("id");

    const monthlyID = (
        await knex(table.PLANS).select("id").where("plan_name", "monthly")
    )[0].id;

    const yearlyID = (
        await knex(table.PLANS).select("id").where("plan_name", "yearly")
    )[0].id;

    console.log(yearlyID);

    await knex(table.SUBSCRIPTIONS).insert([
        {
            user_id: avisID,
            stripe_session_id:
                "my_test_voQJ7O7uddbQJqftTm7fAYb7u8itE2rnjrVShWA4h1f0Js7MruCInyIO",
            stripe_customer_id: "cus_J1CY5nCpWDIAAA",
            stripe_subscription_id: "sub_J0fbFfPmrajV6s",
            current_plan_id: monthlyID,
            plan_start_date: "2020-12-31 00:00:00",
            plan_end_date: "2030-12-31 00:00:00",
            total_price: 20,
        },
        {
            user_id: gigiID,
            stripe_session_id:
                "my_test_aoQJ7O7uddbQJqftTm7fAYb7u8itE2rnjrVShWA4h1f0Js7MruCInyIO",
            stripe_customer_id: "cus_J1CY5nCpWDIABB",
            stripe_subscription_id: "sub_J0fbFfPmrajV6a",
            current_plan_id: monthlyID,
            plan_start_date: "2020-12-31 00:00:00",
            plan_end_date: "2030-12-31 00:00:00",
            total_price: 20,
            is_cancelled: true,
            is_plan_active: false,
        },
        // {
        //     user_id: victorID,
        //     stripe_session_id:
        //         "my_test_boQJ7O7uddbQJqftTm7fAYb7u8itE2rnjrVShWA4h1f0Js7MruCInyIO",
        //     stripe_customer_id: "cus_J1CY5nCpWDIABC",
        //     current_plan_id: yearlyID,
        //     plan_start_date: "2020-12-31 00:00:00",
        //     plan_end_date: "2030-12-31 00:00:00",
        //     payment_method: "VISA",
        //     total_price: 20,
        //     is_cancelled: false,
        //     is_plan_active: true,
        // },
    ]);
}
